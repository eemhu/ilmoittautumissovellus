:: Ilmoittautumissovellus asennusskripti
@echo off

echo --Ilmoittautumissovellus asennusskripti
echo --versio 2019-04-08
echo ----------

set /p port="Syötä portti, josta palvelin kuuntelee yhteyksiä: "
set /p insecure="Käynnistetäänkö palvelin salaamattomassa HTTP-tilassa (ei suositella!) (k/e)?: "

IF EXIST server.js ( GOTO SERVER-FILE-EXISTS ) ELSE ( GOTO SERVER-FILE-NOT-EXISTS )

:SERVER-FILE-EXISTS
echo --Palvelintiedosto OK
IF EXIST node_modules\ ( GOTO SERVER-MODULES-EXISTS ) ELSE ( GOTO SERVER-MODULES-NOT-EXISTS )
::SERVER-FILE-EXISTS END

:SERVER-FILE-NOT-EXISTS
echo --Palvelintiedosto puuttuu, ajathan tämän skriptin siinä kansiossa, jossa se oli alunperin
pause
exit
::SERVER-FILE-NOT-EXISTS END

:SERVER-MODULES-EXISTS
echo --Palvelinmoduulit OK, tarkistetaan päivityksiä
call npm install
echo --Käynnistetään palvelin, paina CTRL-C lopettaaksesi
IF /I "%insecure%" EQU "K" (
    call node server.js --expressPort %port%
) ELSE (
    call node server.js --expressPort %port% --secure
)
pause
exit
::SERVER-MODULES-EXISTS END

:SERVER-MODULES-NOT-EXISTS
echo --Palvelinmoduulit puuttuvat, haetaan ne internetistä...
call npm install
echo --Käynnistetään palvelin, paina CTRL-C lopettaaksesi
IF /I "%insecure%" EQU "K" (
    call node server.js --expressPort %port%
) ELSE (
    call node server.js --expressPort %port% --secure
)
pause
exit
::SERVER-MODULES-NOT-EXISTS END