#!/bin/bash

# Ilmoittautumissovellus käynnistysskripti
echo '-- Ilmoittautumissovellus käynnistysskripti'
echo '-- versio 2019-04-08'
echo '-------------------------'

if [ $(id -u) != 0 ]; then
	echo '--Skripti ei ole käynnissä pääkäyttäjänä, jos koet ongelmia, kokeile suorittaa pääkäyttäjänä (root)'
fi

echo 'Syötä portti, josta palvelin kuuntelee yhteyksiä'
read port
echo 'Käynnistetäänkö palvelin salaamattomassa HTTP-tilassa? (Ei suositella) (k/e)?'
read insecure

if [ -f 'server.js' ]; then
	echo '--Palvelintiedosto OK'
		
	if [ -d 'node_modules/' ]; then
		echo '--Palvelinmoduulit OK, haetaan päivityksiä...'
		npm install
		echo '--Käynnistetään palvelin'
		case $insecure in
			[Kk]* ) node server.js --expressPort $port; break;;
			[Ee]* ) node server.js --expressPort $port --secure; break;;
			* ) node server.js --expressPort $port --secure; break;;
		esac
		exit
	else
		echo '--Palvelinmoduulit puuttuvat, haetaan ne internetistä...'
		npm install
		echo '--Käynnistetään palvelin'
		case $insecure in
			[Kk]* ) node server.js --expressPort $port; break;;
			[Ee]* ) node server.js --expressPort $port --secure; break;;
			* ) node server.js --expressPort $port --secure; break;;
		esac
		exit
	fi
else
	echo '--Palvelintiedosto puuttuu, käynnistithän tämän skriptin alkuperäisestä sijainnista'
	exit
fi
