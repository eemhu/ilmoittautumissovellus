/* 

    Palvelin Kulttuurikimppa-ilmoittautumissovellukseen
    5.2.2019 (updated 25.2.2019)

    GUI SQLite Databasen hallintaan saatavilla osoitteesta
    https://sqlitebrowser.org/dl/

*/
// Node environment (please set to DEVELOPMENT via command-line)
// Use command on Windows: set NODE_ENV=development && nodemon server.js or similar (Linux/Mac: use keyword 'export' in place of 'set')
var environment = process.env.NODE_ENV;
if (environment === null || environment === undefined) {
    environment = "production";
}
var devMode = (environment === "development");
console.log("Environment: " + environment + "\tDev mode: " + devMode);
// Import md5 module
var md5 = require('md5');
// Import file system access
const fs = require('fs');
// Import nodemailer for sending meila
const nodeMailer = require('nodemailer');
// Port used by express (default 8080)
var port = 8080;
// variable which defines if server is supposed to be running in secure/HTTPS mode
var secureMode = false;
// Import https support
var https = require('https');
// Read command-line arguments
process.argv.forEach(function (val, index, array) {
    if (index === 0 || index === 1) {
        /* Ignore node path and script path */
    }
    else if (val === "--expressPort" && !isNaN(array[index + 1])) {
        port = array[index + 1];
        console.log("[Startup] Received argument: " + val + " " + array[index + 1]);
    }
    else if (val === "--secure") {
        secureMode = true;
    }
});
// Import SQLite3 support
const sqlite3 = require('sqlite3').verbose();
// Import Express support
const express = require('express');
const app = express();
// Use CORS middleware w/ express
let cors = require('cors');
app.use(cors());
// Use bodyParser w/ express
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Use express-session middleware w/ express -- Used for tracking logins
let session = require('express-session');
let SQLiteStore = require('connect-sqlite3')(session);
let methodOverride = require('method-override');
let cookieParser = require('cookie-parser');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
    store: new SQLiteStore({
        db: 'sessions.db',
        dir: './db/',
        concurrentDB: true
    }),
    secret: 'xDD8jVp22gbNhTT9zFqqLwr6iiDo0Vaa',
    cookie: {
        maxAge: 24 * 60 * 60 * 1000, /* 1 day */
        secure: secureMode /* secure cookies? */
    }, 
    resave: true,
    saveUninitialized: false
}));

// If secure mode is enabled, load certificates
var secureOptions = undefined;
if (secureMode) {
    try {
        secureOptions = {
            key : fs.readFileSync('certs/server.key','utf8'),
            cert : fs.readFileSync('certs/server.crt','utf8'),
            requestCert : false,
            rejectUnauthorized : false
        }
    }
    catch (certErr) {
        console.log("Error loading certificates !");
        console.log("Server will now close.");
        console.log("Additional details: ", certErr.message);
        process.exit(1);
    }
}

//
// Check authorization before serving static pages
//
app.all('/yllapito/*', function(req, res, next) {
    if (req.session.sid != null && req.session.isAdmin != null && req.session.isAdmin === true) {
        next();
    }
    else {
        res.redirect("/");
    }
});
app.all('/ownAccount/*', function(req, res, next) { if (req.session.sid != null) { next(); } else { res.redirect("/"); } });
app.all('/ownEvents/*', function(req, res, next) { if (req.session.sid != null) { next(); } else { res.redirect("/"); } });
app.all('/kimppatapahtumat/*', function(req, res, next) { if (req.session.sid != null) { next(); } else { res.redirect("/"); } });
app.all('/kasikirja/*', function(req, res, next) { if (req.session.sid != null) { next(); } else { res.redirect("/"); } });
app.all('/ajankohtaista/*', function(req, res, next) { if (req.session.sid != null) { next(); } else { res.redirect("/"); } });

// Serve static pages from /client folder
app.use(express.static(__dirname + '/client'));

// Import bcrypt
const bcrypt = require('bcrypt');
const saltRounds = 10;

/*console.log("Test password generation");
bcrypt.hash("password_here", saltRounds, function(error, hash) {
    console.log(hash);
});*/

// Import node-schedule
var schedule = require('node-schedule');

// Load main SQLite database from /db/main.db
let database = new sqlite3.Database('./db/main.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("\x1b[32m%s\x1b[0m", "[Startup] Connected to disk-based main database file.");
});

/**
 * Get all users, only for admins
 * Returns JSON to client
 */
app.get('/users/get', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin != null) { /* Only admins have access */
        let sql = "SELECT * FROM User"
        database.all(sql, (err, rows) => {
            if (err) console.log("\x1b[31m%s\x1b[0m", "[Database] Error: \t" + err);
            //if (rows) console.log(rows);

            if (rows.length > 0) {
                res.jsonp({
                    auth: true,
                    action: "results",
                    data: { rows }
                });
            }
            else {
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Käyttäjiä ei löytynyt"
                });
            }
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia käyttöoikeuksia"
        });
    }
});

// When client accesses this URL and params, "login"
app.post('/login', (req, res) => {
    if (req.session.sid != null) { res.jsonp({ auth: true, action: "message", data: "Olet jo kirjautuneena sisään." }); }
    else {
        // parameters provided by client
        let email = req.body.email;
        let pass = req.body.pass;
        // sql query template
        let sql = "SELECT * FROM User WHERE email=$email";
        // parameters for sql query template
        let params = {
            $email: email
        };
        //console.log("SQL Params: " + params.$email);
        // perform sql query on connected database and send response based on query result
        database.all(sql, params, (err, rows) => {
            if (err) console.log("\x1b[31m%s\x1b[0m", "[Database] Error: \t" + err);
            //if (rows) console.log(rows);

            if (rows.length > 0) {
                bcrypt.compare(pass, rows[0].password, function (hash_err, hash_resp) { /* Checking index 0 is enough, emails are unique */
                    if (hash_resp) {
                        req.session.sid = email;

                        if (!!rows[0].admin) { // Admin
                            req.session.isAdmin = true;
                            res.jsonp({
                                auth: true,
                                action: "redirect",
                                data: "/yllapito"
                            });
                        }
                        else { // Normal user
                            req.session.userID = rows[0].id;
                            res.jsonp({
                                auth: true,
                                action: "redirect",
                                data: "/kimppatapahtumat"
                            });
                        }

                        console.log("[Login] New login\t" + JSON.stringify(req.session));
                    }
                    else {
                        res.jsonp({
                            auth: false,
                            action: "message",
                            data: "Virheellinen salasana"
                        });
                        console.log("\x1b[31m%s\x1b[0m", "[Login] Login attempt failed, invalid password.");
                    }
                });
            }
            else {
                res.jsonp({
                    auth: false,
                    action: "message",
                    data: "Virheellinen käyttäjätunnus"
                });
                console.log("\x1b[31m%s\x1b[0m", "[Login] Login attempt failed, incorrect login details");
            }
        });
    }
});

// Remove user
app.post('/users/remove', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin != null) { /* Admin only */
        let email = req.body.email;
        let checkSql = "SELECT COUNT(*) FROM User WHERE email=$email AND permanent=0";
        let sql = "DELETE FROM User WHERE email=$email";
        let params = {
            $email: email
        };

        console.log("[Maintenance] Admin initiated user removal process for the email:\t" + email);
        database.all(checkSql, params, function (err, rows) {
            if (rows[0]["COUNT(*)"] > 0) {
                // email ok
                database.run(sql, params, function (err) {
                    if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
                    console.log("[Database] User " + email + " was removed successfully from database.");
                    res.jsonp({
                        auth: true,
                        action: "message",
                        data: "Käyttäjä " + email + " poistettiin onnistuneesti"
                    });
                });
            }
            else {
                // wrong email
                console.log("[Database] The email " + email + " was not found in the database, thus nothing was removed.");
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Sähköposti on virheellinen, tarkista syöte"
                });
            }
        });
    }
    else {
        // no auth
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia käyttöoikeuksia tämän operaation suorittamiseen."
        });
    }
});

// main processing function for inviting users
async function processUserInvitation(req, res_callback) {
    let inviteEmail = req.body.email;
    let isEmailValid = await checkEmailValidity(inviteEmail);
    let isAdmin = req.body.isAdmin;

    if (isEmailValid/* || inviteEmail*/) {
        let inviteId = await generateInviteId(inviteEmail);
        if (!devMode) {
            console.log("Production environment detected, actually sending mail");
            let sentMail = await sendEmail(inviteEmail, inviteId);
        }
        let insertToDatabaseSuccess = await insertInvitationToDatabase(inviteEmail, inviteId, isAdmin);

        if (insertToDatabaseSuccess) {
            res_callback.jsonp({
                auth: true,
                action: "message",
                data: "Kutsu lähetettiin sähköpostiin " + inviteEmail// + ", käytä rekisteröintiin osoitetta /rekisteroi?inviteId=" + inviteId + "&email=" + inviteEmail
            });
        }
        else {
            res_callback.jsonp({
                auth: true,
                action: "message",
                data: "Virhe lähetettäessä kutsua"
            });
        }
    }
    else {
        res_callback.jsonp({
            auth: true,
            action: "message",
            data: "Sähköposti on virheellisessä muodossa"
        });
    }
}

// generate invite id from email
function generateInviteId(email) {
    return new Promise((resolve, reject) => {
        let id = md5(email);
        resolve(id);
    });
}

// insert invitation to db
function insertInvitationToDatabase(email, inviteId, isAdmin) {
    return new Promise((resolve, reject) => {
        database.run("INSERT INTO Invite(email, inviteId, isAdmin) VALUES (?,?,?)", email, inviteId, isAdmin, function (err) {
            if (err) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    });
}

async function sendEmail(to, inviteId) {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodeMailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: "kulttuurikimppajoensuu@gmail.com", //account.user, // generated ethereal user
            pass: "kulttuuri123kimppa" //account.pass // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Kulttuurikimppa 👻" <kulttuurikimppajoensuu@gmail.com>', // sender address
        to: to, // list of receivers
        subject: "Tervetuloa kulttuurikamuksi", // Subject line
        text: "Sinut on kutsuttu rekisteröitymään kulttuurikamuksi!\nRekisteröidy osoitteessa: http://localhost:8080/rekisteroi?inviteId=" + inviteId + "&email=" + to, // plain text body
        //html: "<b>Hello world?</b>" // html body
    };

    // send mail with defined transport object
    let info = await transporter.sendMail(mailOptions)

    console.log("Message sent: %s", info.messageId);
}

async function sendPassChangeEmail(to, changeId) {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodeMailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: "kulttuurikimppajoensuu@gmail.com", //account.user, // generated ethereal user
            pass: "kulttuuri123kimppa" //account.pass // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Kulttuurikimppa 👻" <kulttuurikimppajoensuu@gmail.com>', // sender address
        to: to, // list of receivers
        subject: "Kulttuurikamun salasanan vaihto", // Subject line
        text: "Olet pyytänyt salasanan vaihtoa kulttuurikimppa käyttäjällesi!\nVaihda salasana osoitteessa: http://localhost:8080/passChange?changeId=" + changeId + "&email=" + to
        + "\nLinkki on voimassa seuraavat 24h."
        + "\n\nJos et yrittänyt vaihtaa salasanaasi, voit jättää tämän viestin huomioimatta.", // plain text body
        //html: "<b>Hello world?</b>" // html body
    };

    // send mail with defined transport object
    let info = await transporter.sendMail(mailOptions)

    console.log("Message sent: %s", info.messageId);
}

// Add permission for password change to database + send password change email
app.post('/users/passChangePermission', (req, res)  => {
    
    let email = req.body.email;
    let params = {
        $email: email
    };

    let sql = "SELECT * FROM User WHERE email=$email";
    database.all(sql, params, async (err, rows) => {
        if (err) console.log("[Database] Error: \t" + err);

        if (rows.length > 0) {

            let changeId = await createId();

            database.run('INSERT INTO PassChange VALUES (?, "'+ email +'", ' + "date('now'), " + '"' + changeId + '")', async function(err) {
                if (err) return console.log(err.message);
                console.log("[Database] Updated");
                console.log("Salasanan vaihtopyyntö lisätty käyttäjälle " + email);
                if (!devMode) {
                    await sendPassChangeEmail(email, changeId);
                }
                else {
                    console.log("Not sending pass change email in dev mode. Details: %s, %s", email, changeId);
                }

                res.jsonp({
                    auth: true,
                    action: "success",
                    data: "Vaihtopyyntö luotiin onnistuneesti ja lähetettiin sähköpostiisi."
                });
            });
        }
        else {
            res.jsonp({
                auth: true,
                action: "message",
                data: "Käyttäjätiliä tällä sähköpostilla ei löytynyt"
            });
        }
    });
});

//generate 24 digit random id
function createId(){
    var id = Math.random().toString(36).substr(2, 8) + Math.random().toString(36).substr(2, 8) + Math.random().toString(36).substr(2, 8);
    console.log("generoitu id on" + id);
    return id;
}

// Check from database whether request "changeId" and "email" pair is correct
app.post('/users/passCheckPair', (req, res)  => {

    let changeId = req.body.changeId;
    let email = req.body.email;

    console.log("changeId: " + changeId + " email: "+ email);

    let sql = 'SELECT email FROM PassChange WHERE changeId IS "' + changeId +'" AND email IS "' + email + '"'; 
        database.all(sql, (err, rows) => {
            if (err) console.log("[Database] Error: \t" + err);

            if (rows.length > 0) {
                res.jsonp({
                    auth: true,
                    action: "results",
                    data: rows.length
                });
            }
            else {
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Salasanan vaihtopyyntöä ei löytynyt"
                });
            }
        });
});

//changes user's password 
app.post('/users/changePass', async (req, res) => {

    let pass = req.body.pass;
    let email = req.body.email;

    let newHashPass = await hashNewPass(pass);
    console.log("Password: " + newHashPass, "email: " + email);
    let sql = 'UPDATE User SET password = "' + newHashPass + '" WHERE email = "' + email + '"';

    database.run(sql, (err, rows) => {

        if (err) console.log("\x1b[31m%s\x1b[0m", err.message);

        console.log("[Database] Pass changed");

        if(err) {
            res.jsonp({
                auth: true,
                action: "message",
                data: "Salasanan vaihdossa virhe"
            });
        }
        else{
            res.jsonp({
                auth: true,
                action: "message",
                data: "Salasana vaihdettu onnistuneesti. Voit nyt kirjautua uudella salasanallasi."
            });
            // When passChange was success then DELETE "used" email and id pair
            let sqldel = 'DELETE FROM PassChange WHERE email ="' + email + '"';

            database.run(sqldel, (err, rows) => {
                if (err) {
                    console.log("\x1b[31m%s\x1b[0m", err.message);
                }
                else {
                    console.log("[DATABASE] deleting passchanges, data:" + { rows });
                }
            });
        }
    });
});

// Add user to invite table and send invite email
app.post('/users/invite', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin) {
        processUserInvitation(req, res);
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole oikeuksia kutsua käyttäjiä."
        });
    }
});

// Allow logged in user to edit account details
app.post('/user/edit', (req, res) => {
    if (req.session.sid != null && req.session.userID != null) {
        processUserEdit(req, res);
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia käyttöoikeuksia tai käyttäjä id puuttuu."
        });
    }
});

// Check name, phone, pass and email
function checkParams(name, phone, pass, email) {
    return new Promise((resolve, reject) => {
        let emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        let phoneRegex = /^\d+$/;

        let phoneTest = phoneRegex.test(phone);
        let emailTest = emailRegex.test(email);
        let passTest = pass.length >= 6;
        let nameTest = name.length >= 2;

        console.log("Test: phone/email/pass/name  " + phoneTest + "/" + emailTest + "/" + passTest + "/" + nameTest);

        if (!phoneTest || !emailTest || !passTest || !nameTest) {
            resolve(false);
        }
        else {
            resolve(true);
        }
    });
}

// Check only email (for invite sending)
function checkEmailValidity(email) {
    return new Promise((resolve, reject) => {
        let emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        let emailTest = emailRegex.test(email);

        console.log("Testing email validity. Success: " + emailTest);
        resolve(emailTest);
    });
}

// Process POST /user/edit
// Required for async
async function processUserEdit(req, res_callback) {
    // Current, non-modified account details
    let currentUserEmail = req.session.sid;
    let currentUserID = req.session.userID;
    let currentPass = req.body.oldPass;

    // New account details
    let name = req.body.name;
    let phone = req.body.phoneNumber;
    let pass = req.body.pass;
    let email = req.body.email;

    // Validate params
    let validParams = await checkParams(name, phone, pass, email);

    if (validParams) {
        // Check if current password is correct
        let passCheckSql = "SELECT * FROM User WHERE id=$uid AND email=$email";
        let params = {
            $uid: currentUserID,
            $email: currentUserEmail
        };

        let passCorrect = await checkOldPass(passCheckSql, params, currentPass);

        if (passCorrect) {
            let newPassHash = await hashNewPass(pass);

            let modifySql = "UPDATE User SET name = $name, phoneNumber = $phone, password = $hashedPass, email = $email WHERE id = $uid";
            let params2 = {
                $name: name,
                $phone: phone,
                $hashedPass: newPassHash,
                $email: email,
                $uid: currentUserID
            };

            let modifySuccess = await modifyUser(modifySql, params2);

            if (modifySuccess) {
                res_callback.jsonp({
                    auth: true,
                    action: "message",
                    data: "Tiedot muutettiin."
                });
            }
            else {
                res_callback.jsonp({
                    auth: false,
                    action: "message",
                    data: "Tietoja ei muutettu, virhetilanne"
                });
            }
        }
        else {
            res_callback.jsonp({
                auth: false,
                action: "message",
                data: "Väärä vanha salasana"
            });
        }
    }
    else {
        res_callback.jsonp({
            auth: false,
            action: "message",
            data: "Virheelliset uudet tiedot: Varmista, että nimessäsi on vähintään 2 merkkiä, salasana on vähintään 6 merkkiä, puhelinnumerosi sisältää vain numeroita ja sähköpostisi on oikeassa muodossa."
        });
    }
}

// Check old pass w/ async
function checkOldPass(passCheckSql, params, currentPass) {
    return new Promise((resolve, reject) => {
        database.all(passCheckSql, params, function (err, rows) {
            if (err) return console.log(err.message);
            bcrypt.compare(currentPass, rows[0].password, (hash_err, hash_resp) => {
                if (hash_err || !hash_resp) {
                    resolve(false);
                }
                else {
                    resolve(true);
                }
            });
        });
    });
}

// Hash new pass w/ async
function hashNewPass(newPass) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(newPass, saltRounds, (hash_err, hash_resp) => {
            if (hash_err) return console.log("\x1b[41m%s\x1b[0m", hash_err.message);

            resolve(hash_resp);
        });
    });
}

// Perform user edit w/ async
function modifyUser(modifySql, params) {
    return new Promise((resolve, reject) => {
        database.run(modifySql, params, (err) => {
            console.log(params.$name + " " + params.$phone + " " + params.$hashedPass + " " + params.$email + " " + params.$uid);
            if (err) {
                console.log("\x1b[41m%s\x1b[0m", "ModifySQL:" + err.message);
                resolve(false);
            } else {
                resolve(true);
            }

        });
    });
}

// Edit website content (admin only)
// TODO: testing
app.post('/content/edit', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin) {
        let docToEdit = req.body.edit;
        let html = req.body.html;

        performEdit(res, docToEdit, html);
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia käyttöoikeuksia tämän toiminnon toteuttamiseen."
        });
    }
});

// perform site editing
async function performEdit(res_callback, docToEdit, html) {
    let saveSuccess = await saveHtmlToFile(html, docToEdit);

    if (saveSuccess) {
        res_callback.jsonp({
            auth: true,
            action: "message",
            data: "Tiedot tallennettiin onnistuneesti."
        });
    }
    else {
        res_callback.jsonp({
            auth: true,
            action: "error",
            data: "Tietoja ei tallennettu onnistuneesti."
        });
    }
}

// save html code to target file
function saveHtmlToFile(html, docToEdit) {
    return new Promise((resolve, reject) => {
        let target = undefined;

        if (docToEdit === "frontPage") {
            target = "client/modifiableContent/frontPage.html";
        }
        else if (docToEdit === "contacts") {
            target = "client/modifiableContent/contacts.html";
        }
        else if (docToEdit === "page_1") {
            target = "client/modifiableContent/page_1.html";
        }
        else if (docToEdit === "page_2") {
            target = "client/modifiableContent/page_2.html";
        }
        else {
            resolve(false);
        }

        if (fs.existsSync(target)) {
            fs.writeFile(target, html, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        }
        else {
            console.log("file does not exist");
            resolve(false);
        }
    });
}

// Get current editable content
app.get('/content/get', (req, res) => {
    let type = req.query.type;
    console.log("Get old content: " + type);
    performContentGet(res, type);
});

// get old edited content
async function performContentGet(res_callback, type) {
    let file = undefined;
    if (type == "frontPage") {
        file = "client/modifiableContent/frontPage.html";
    }
    else if (type == "contacts") {
        file = "client/modifiableContent/contacts.html";
    }
    else if (type == "page_1") {
        file = "client/modifiableContent/page_1.html";
    }
    else if (type == "page_2") {
        file = "client/modifiableContent/page_2.html";
    }
    else {
        res_callback.jsonp({
            auth: true,
            action: "error",
            data: "Virheellinen sivustotyyppi. Yritä myöhemmin uudelleen."
        });
        return;
    }

    let htmlFile = await readHtmlFromFile(file);

    if (htmlFile.success) {
        res_callback.jsonp({
            auth: true,
            action: "results",
            data: htmlFile.rawHtml
        });
    }
    else {
        res_callback.jsonp({
            auth: true,
            action: "error",
            data: "Virhe haettaessa sisältöä. Yritä myöhemmin uudelleen."
        });
    }
}

// read html from file
function readHtmlFromFile(file) {
    return new Promise((resolve, reject) => {
        if (fs.existsSync(file)) {
            let html = fs.readFileSync(file).toString();
            resolve({ success: true, rawHtml: html });
        }
        else {
            resolve({ success: false });
        }
    });
}

// check validity of invite id or if account exists already
function checkValidity(email, type, inviteId) {
    return new Promise((resolve, reject) => {
        let sql, params = undefined;
        if (type === "doesInviteExist" && inviteId !== null) {
            console.log("Does invite exist");
            sql = "SELECT * FROM Invite WHERE email = $email AND inviteId = $inviteId;";
            params = { $email: email, $inviteId: inviteId };
        }
        else if (type === "doesAccountAlreadyExist") {
            console.log("Does account exist");
            sql = "SELECT COUNT(*) FROM User WHERE email = $email;";
            params = { $email: email };
        }
        else {
            resolve(false);
        }

        database.all(sql, params, (err, rows) => {
            if (type === "doesInviteExist" && rows.length > 0) {
                console.log("True");
                resolve({success: true, isAdmin : rows[0]["isAdmin"]});
            }
            else if (type === "doesAccountAlreadyExist" && rows[0]["COUNT(*)"] > 0) {
                console.log("True");
                resolve(true);
            }
            else {
                if (type === "doesInviteExist") {
                    resolve({success : false});
                }
                else {
                    resolve(false);
                }
            }
        });
    });
}

// remove invite from db
function removeInviteFromDatabase(email) {
    return new Promise((resolve, reject) => {
        let sql = "DELETE FROM Invite WHERE email = $email;";
        let params = { $email: email };

        database.run(sql, params, (err) => {
            if (err) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    });
}

// add user to db
function addNewUserToDatabase(name, phone, hashedPass, email, isAdmin, permanent) {
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO User(name, phoneNumber, email, password, admin, permanent) VALUES(?,?,?,?,?,?)";

        database.run(sql, name, phone, email, hashedPass, isAdmin, permanent, (err) => {
            if (err) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    });
}

// main processing of user register
async function processUserRegister(req, res_callback) {
    let email = req.body.email;
    let name = req.body.name;
    let phone = req.body.phoneNumber;
    let pass = req.body.pass;
    let passAgain = req.body.passAgain;
    let isAdmin = 0;
    let inviteId = req.body.inviteId;
    let permanent = 0;

    let hashedPass = "";

    let validParams = await checkParams(name, phone, pass, email);
    let samePass = await (pass === passAgain);

    if (validParams && samePass) {
        let inviteExists = await checkValidity(email, "doesInviteExist", inviteId);
        let accountExists = await !checkValidity(email, "doesAccountAlreadyExist", null);
        if (inviteExists.success && !accountExists) {
            isAdmin = await inviteExists.isAdmin;
            hashedPass = await hashNewPass(pass);
            let addUserSuccess = await addNewUserToDatabase(name, phone, hashedPass, email, isAdmin, permanent);
            if (addUserSuccess) {
                let removed = await removeInviteFromDatabase(email);
                if (!removed) {
                    res_callback.jsonp({
                        auth: false,
                        action: "message",
                        data: "Virhe: Kutsun poisto epäonnistui"
                    });
                }
                else {
                    res_callback.jsonp({
                        auth: true,
                        action: "message",
                        data: "Rekisteröinti onnistui"
                    });
                }
            }
            else {
                res_callback.jsonp({
                    auth: false,
                    action: "message",
                    data: "Virhe: Käyttäjän lisäys epäonnistui"
                });
            }
        }
        else {
            res_callback.jsonp({
                auth: false,
                action: "message",
                data: "Virhe: Kutsua ei ole olemassa tai tili on jo olemassa kyseiselle sähköpostille."
            });
        }
    }
    else {
        res_callback.jsonp({
            auth: false,
            action: "message",
            data: "Virhe: Tarkista kentät!"
        });
    }
}

// Register new user with invited email
app.post('/users/new', (req, res) => {
    processUserRegister(req, res);
});

// Create new event from admin console
// Multiple questions JSON is not processed server side
app.post('/event/new', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin != null) { /* Admin only */
        // POST variables
        let eventName = req.body.eventName;
        let eventText = req.body.eventText;
        let numberOfVolunteers = req.body.numberOfVolunteers;
        let eventDate = req.body.eventDate;
        let eventType = req.body.eventType;

        if (eventName && eventText && numberOfVolunteers && eventDate && numberOfVolunteers > 0) {
            // sql query for inserting new event
            let sql = "INSERT INTO Event(numberOfVolunteers, eventText, name, date, type) VALUES(?,?,?,?,?)";
            database.run(sql, numberOfVolunteers, eventText, eventName, eventDate, eventType, function (err) {
                if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
                res.jsonp({
                    auth: true,
                    action: "success",
                    data: "Tapahtuma luotiin onnistuneesti"
                });
            });
        }
        else if (numberOfVolunteers <= 0) {
            res.jsonp({
                auth : true,
                action : "invalidInfo",
                data : "Vapaaehtoisten lukumäärä tulee olla vähintään yksi (1) kappale."
            });
        }
        else {
            res.jsonp({
                auth : true,
                action : "missingInfo",
                data : "Tarvittavia tietoja puuttuu."
            });
        }

    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia oikeuksia tämän operaation tekemiseen."
        });
    }
});

// Edit selected event
app.post('/event/edit', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin) {
        let eventID = req.body.eventID;
        let numberOfVolunteers = req.body.numberOfVolunteers;
        let eventText = req.body.eventText;
        let name = req.body.eventName;
        let date = req.body.eventDate;

        if (numberOfVolunteers <= 0) {
            res.jsonp({
                auth : true,
                action : "message",
                data : "Vapaaehtoisten lukumäärän tulee olla vähintään yksi (1) kappale."
            });
        }
        else {
            let sql = "UPDATE Event SET numberOfVolunteers = $numberOfVolunteers, eventText = $eventText, name = $name, date = $date WHERE id = $eventID";
            let params = {
                $numberOfVolunteers: numberOfVolunteers,
                $eventText: eventText,
                $name: name,
                $date: date,
                $eventID: eventID
            };

            database.run(sql, params, (err) => {
                if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Tapahtuman tietoja muutettiin"
                });
            });
        }
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole oikeuksia tähän toimenpiteeseen."
        });
    }
});

// Remove selected event
app.post('/event/remove', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin) {
        let eventID = req.body.eventID;
        let sql = "DELETE FROM Event WHERE id=$eventID";
        let params = {
            $eventID : eventID
        };

        if (params.$eventID !== null && !isNaN(params.$eventID)) {
            database.run(sql, params, (err) => {
                if (err) {
                    res.jsonp({
                        auth : true,
                        action : "error",
                        data : "Virhe: " + err.message
                    });
                }
                else {
                    res.jsonp({
                        auth : true,
                        action : "message",
                        data : "Tapahtuma poistettiin onnistuneesti."
                    });
                }
            });
        }
        else {
            res.jsonp({
                auth : true,
                action : "message",
                data : "Tapahtumatiedot olivat virheelliset, yritä myöhemmin uudelleen."
            });
        }
    }
    else {
        res.jsonp({
            auth : false,
            action : "message",
            data : "Sinulla ei ole oikeuksia poistaa tapahtumaa."
        });
    }
});

// Log out from session
// Destroys session from session database
app.get('/logout', (req, res) => {
    if (req.session.sid != null) {
        req.session.destroy((err) => {
            if (err) {
                console.log("\x1b[31m%s\x1b[0m", "[Logout] Error:\t" + err);
                res.jsonp({
                    auth: true,
                    action: "error",
                    data: "Error: " + err.message
                });
            }
            else {
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Kirjauduttiin ulos onnistuneesti"
                });
                console.log("[Logout] User logged out.");
            }
        });
    }
    else {
        //res.send("Et ole kirjautuneena sisään.");
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään"
        });
    }

});

// If authorized, return results of SQL query
// Authorization is checked from express-session
app.get('/events/get', (req, res) => {
    if (req.session.sid != null) {
        let sql = "SELECT * FROM Event " +
            "ORDER BY date";
        database.all(sql, (err, rows) => {
            if (rows.length <= 0) {
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Ei tapahtumia"
                })
            }
            else {
                res.jsonp({
                    auth: true,
                    action: "results",
                    data: { rows }
                });
            }
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään"
        });
    }
});

// If authorized, and correct event ID, move to event page
app.get('/event/view', (req, res) => {
    if (req.session.sid != null) {
        console.log("View event id: " + req.query.eventID);
        let sql = "SELECT Event.id, Event.name, Event.eventText, Event.date, COUNT(*), Event.numberOfVolunteers " +
            "FROM Signup " +
            "INNER JOIN Event ON Signup.eventID = Event.id " +
            "WHERE Event.id = $eventID";
        let params = {
            $eventID: req.query.eventID
        };
        database.all(sql, params, function (err, event) {
            if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
            res.jsonp({
                auth: true,
                action: "results",
                data: { event }
            });
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään."
        });
    }
});

// If authorized, view all events and signups at once
app.get('/events/view', (req, res) => {
    if (req.session.sid != null) {
        console.log("View all events");
        let sql = "SELECT id, name, eventText, numberOfVolunteers, date, expired, max(signupAmount) AS signupAmount, type FROM signupAmountsView GROUP BY id ORDER BY date;";
        database.all(sql, function (err, rows) {
            if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
            res.jsonp({
                auth: true,
                action: "results",
                data: { rows }
            });
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään."
        });
    }
});

// If authorized, and correct event ID, return event's signed up users
app.get('/event/viewSignups', (req, res) => {
    if (req.session.sid != null) {
        console.log("View signups in event id: " + req.query.eventID);
        let sql = "SELECT DISTINCT User.name FROM User " +
            "INNER JOIN Signup ON User.id = Signup.userID " +
            "INNER JOIN Event ON Signup.eventID = Event.id " +
            "WHERE Event.id = $eventID " +
            "ORDER BY User.name";
        let params = {
            $eventID: req.query.eventID
        };
        database.all(sql, params, function (err, signups) {
            if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
            res.jsonp({
                auth: true,
                action: "results",
                data: { signups }
            });
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään."
        });
    }
});

// View signups of user
app.get('/user/viewSignups', (req, res) => {
    if (req.session.sid != null) {
        console.log("View eventIDs of signed in user's signups (" + req.session.sid + ")");
        let sql = "SELECT Event.id FROM User " +
            "INNER JOIN Signup ON User.id = Signup.userID " +
            "INNER JOIN Event ON Signup.eventID = Event.id " +
            "WHERE User.email = $userEmail " +
            "GROUP BY Event.id " +
            "ORDER BY Event.date";
        let params = {
            $userEmail: req.session.sid
        };
        database.all(sql, params, function (err, signups) {
            if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
            res.jsonp({
                auth: true,
                action: "results",
                data: { signups }
            });
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään."
        });
    }
});

// If authorized, return results of SQL query
// Authorization is checked from express-session
app.get('/signups/get', (req, res) => {
    if (req.session.sid != null && req.session.isAdmin != null) { /* Only admins have access */
        let sql = "SELECT * FROM Signup";
        database.all(sql, (err, rows) => {
            if (rows.length <= 0) {
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Ei ilmoittautumisia"
                })
            }
            else {
                res.jsonp({
                    auth: true,
                    action: "results",
                    data: { rows }
                });
            }
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Et ole kirjautuneena sisään"
        });
    }
});

// Admin only: remove a signup based on its ID
app.post('/signups/delete', (req, res) => { 
    if (req.session.sid != null && req.session.isAdmin != null) { /* Only admins have access */
        let signupID = req.body.signupID;
        if (signupID == null) {
            console.log("\x1b[31m%s\x1b[0m", "[SignupsDelete] signupID is missing, cannot delete signup");
            res.jsonp({
                auth: false,
                action: "message",
                data: "Ilmoittautumisen poisto epäonnistui, tietoja puuttuu"
            });
        } else {
            let sql = "DELETE FROM Signup WHERE id=$signupID";
            let params = {
                $signupID: signupID
            };
            database.run(sql, params, (err) => {
                if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Poistettiin ilmoittautuminen"
                });
            });
        }
    } else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole tarvittavia käyttöoikeuksia"
        });
    }
});

// Process POST request for signing up for an event
// Request should contain eventID
// Signing up only succeeds if eventID exists in Events and user has not yet signed up with this eventID
app.post('/event/signup', (req, res) => {
    if (req.session.sid != null) { // check if user is logged in
        let userID = req.session.userID;
        let eventID = req.body.eventID;
        let userMessage = req.body.userMessage;
        if (eventID == null || userID == null) {
            console.log("\x1b[31m%s\x1b[0m", "[EventSignUp] userID or eventID is missing, cannot sign up for event");
            res.jsonp({
                auth: false,
                action: "message",
                data: "Tapahtuma-ilmoittautuminen epäonnistui, tietoja puuttuu"
            });
        } else {
            // check if eventID exists
            let sql = "SELECT COUNT(*) FROM Event WHERE id = $eventID;";
            let params = {
                $eventID: eventID
            };
            database.all(sql, params, (err, rows) => {
                if (err) console.log("\x1b[31m%s\x1b[0m", "[Database] Error: \t" + err);

                if (rows[0]["COUNT(*)"] === 1) { // eventID exists

                    // check if there's less signups than the required amount
                    let sql = "SELECT (COUNT(*) < Event.numberOfVolunteers) AS canSignup " +
                        "FROM Signup " +
                        "INNER JOIN Event ON Signup.eventID = Event.id " +
                        "WHERE eventID = $eventID";
                    let params = {
                        $eventID: eventID
                    };
                    database.all(sql, params, (err, rows) => {
                        if (err) console.log("\x1b[31m%s\x1b[0m", "[Database] Error: \t" + err);
                        if (rows[0]["canSignup"] === 1 || rows[0]["canSignup"] === null) { // is there room to signup in the event?
                            // check if user has already signed up with this eventID
                            sql = "SELECT COUNT(*) FROM Signup WHERE eventID = $eventID AND userID = $userID;";
                            params = {
                                $eventID: eventID,
                                $userID: userID
                            };
                            //console.log("SQL Params: " + params.$eventID + " " + params.$userID);
                            database.all(sql, params, (err, rows) => {
                                if (err) console.log("\x1b[31m%s\x1b[0m", "[Database] Error: \t" + err);

                                if (rows[0]["COUNT(*)"] === 0) {
                                    // insert a row into Signup
                                    database.run("INSERT INTO Signup(userID, eventID, userMessage) VALUES(?, ?, ?)", req.session.userID, eventID, userMessage, function (err) {
                                        if (err) {
                                            return console.log("\x1b[31m%s\x1b[0m", err.message);
                                        }
                                        console.log("[Database] Inserted row into Signup");
                                        res.jsonp({
                                            auth: true,
                                            action: "message",
                                            data: "Tapahtuma-ilmoittautuminen onnistui"
                                        });
                                    });
                                } else { // user has already signed up for this event
                                    console.log("[Database] Combination of userID and eventID already found in Signup");
                                    res.jsonp({
                                        auth: true,
                                        action: "message",
                                        data: "Olet jo ilmoittautuneena tapahtumaan."
                                    });
                                }
                            });
                        } else {
                            console.log("\x1b[31m%s\x1b[0m", "[Database] Event is full.");
                            res.jsonp({
                                auth: true,
                                action: "message",
                                data: "Epäonnistui, tapahtumassa on jo tarpeeksi ilmoittautuneita."
                            });
                        }
                    });
                }
            });
        }
    }
});

// delete signup
app.post('/event/signup/delete', (req, res) => {
    if (req.session.sid != null) { // check if user is logged in
        let userID = req.session.userID;
        let eventID = req.body.eventID;
        if (eventID != null && userID != null) {
            console.log("[Database] Removing sign up!");
            let sql = "DELETE FROM Signup WHERE userID=$uid AND eventID=$eid";
            let params = {
                $uid: userID,
                $eid: eventID
            };
            database.run(sql, params, (err) => {
                if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
                res.jsonp({
                    auth: true,
                    action: "message",
                    data: "Poistettiin ilmoittautuminen"
                });
            });
        }

    }
});

// Check authorization
app.get('/checkAuth', (req, res) => {
    res.jsonp({
        auth: (req.session.sid != null),
        adminAuth: (req.session.isAdmin),
        data: req.session.sid
    });
});

// Get user details
app.get('/user/get', (req, res) => {
    if (req.session.sid != null) {
        let sql = "SELECT name, email, phoneNumber FROM User WHERE email=$email";
        let params = {
            $email: req.session.sid
        };

        database.all(sql, params, (err, userInfo) => {
            if (err) return console.log("\x1b[31m%s\x1b[0m", err.message);
            res.jsonp({
                auth: true,
                action: "results",
                data: { userInfo }
            });
        });
    }
    else {
        res.jsonp({
            auth: false,
            action: "message",
            data: "Sinulla ei ole oikeuksia tämän toiminnon suorittamiseen."
        });
    }
});

// listen on port defined by variable "port" (default 8080)
if (secureMode) {
    var server = https.createServer(secureOptions, app);
    server.listen(port);
    console.log("\x1b[32m%s\x1b[0m", `🔒   HTTPS Server running on port ${port}!`);
}
else {
    app.listen(port, '0.0.0.0', () => console.log("\x1b[33m%s\x1b[0m", `🔓   HTTP (Insecure!) server running on port ${port}!`));
}
// Check every 24 hours (every time clock hits 23:55) if events have been expired
// Expired event = date is older than current
let cronSchedule = "0 55 23 * * *";
var expiredEventChecker = schedule.scheduleJob(cronSchedule, function () {
    console.log("[Schedule] Updating event expiration dates");

    let sql = "UPDATE Event SET expired = 1 WHERE date < date('now') AND expired != 1;";

    database.run(sql, (err) => {
        if (err) {
            console.log("\x1b[31m%s\x1b[0m", err.message);
        }
        else {
            console.log("Scheduler success.");
        }
    });

    let sql2 = "DELETE FROM PassChange WHERE date < date('now')";

    database.run(sql2, (err) => {
        if (err) {
            console.log("\x1b[31m%s\x1b[0m", err.message);
        }
        else {
            console.log("Scheduler success.");
        }
    });
});
