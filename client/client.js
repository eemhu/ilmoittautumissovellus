// Client-side JavaScript Ilmoittautumissovellukselle

var ownSignups;

const urlParams = new URLSearchParams(window.location.search);

// Check authorization
// GET /checkAuth returns following details:
// auth : true/false - Does user have basic access privileges? (not admin)
// adminAuth : true/false - Does user have admin privileges?
// data : user's email address
function checkAuth() {
    $.get("/checkAuth", function (data) {
        console.log("checkAuth(): Sending GET to the server");
    }).done(function (data) {
        console.log("checkAuth(): Received data: ", data);
        if (data.auth === true) {
            console.log("checkAuth(): Käyttäjä on kirjautunut järjestelmään.");

            // When present, and user is logged in, set column width to col-lg-12.
            let indexColumnWide = document.getElementById("column_wide");
            if (indexColumnWide !== null) {
                indexColumnWide.className = "col-lg-12";
            }

            // Enable menu visible on the upper-right of the page (in navbar)
            $("#dropdownMenuButton").text(data.data);
            $("#dropdownMenuButton").removeClass("disabled");
            $("#userEventsButton").removeClass("d-none");
            $("#userEventsButton2").removeClass("d-none");

            // Enable links to the next of the logo (in navbar)
            //$("#hide1").removeClass("d-none");
            $("#hide2").removeClass("d-none");
            $("#hide3").removeClass("d-none");
            $("#hide4").removeClass("d-none");
            $("#hide5").removeClass("d-none");

        }
        else if (data.auth === false) {
            console.log("checkAuth(): Käyttäjä EI OLE kirjautunut järjestelmään.");

            // Display login form, if user is not logged in
            $("#login").removeClass("d-none");
        }

        // Display maintenance option in menu, hide events option in menu - if admin is logged in
        if (data.adminAuth === true) {
            console.log("checkAuth(): Käyttäjä on kirjautunut ylläpitäjänä.");
            $("#maintenanceButton").removeClass("d-none");
            $("#userEventsButton").addClass("d-none");
            $("#userEventsButton2").addClass("d-none");
            
        }
    });
}

// Returns boolean value representing the admin privileges status
// Used to secure maintenance page
function returnAdminAuthState() {
    return new Promise((resolve, reject) => {
        $.get("/checkAuth", function (data) {
            console.log("checkAuth(): Sending GET to the server");
        }).done(function (data) {
            console.log("checkAuth(): Received data: ", data);
            if (data.auth && data.adminAuth) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        });
    });  
}

// Check if mobile, and use mobile-optimized css
function checkDevice() {
    let np = navigator.platform;

    if (np.startsWith("iP") || np.startsWith("Android") || np.startsWith("Linux arm")) {
        console.log("Detected platform: iDevice, Android or ARM based Linux");
        document.getElementById("sitecss").setAttribute("href", "/css/style_mobile.css");
    }
    else {
        console.log("Detected platform: Desktop");
        // css defaults to desktop, no need to change
    }
}

// load contacts page
function loadContacts() {
    $("#editableContactsPageContent").load("../modifiableContent/contacts.html");
}

// load custom pages
function loadPage_1() {
    $("#editablePage_1Content").load("../modifiableContent/page_1.html");
}

function loadPage_2() {
    $("#editablePage_2Content").load("../modifiableContent/page_2.html");
}


// will be loaded on body onload (html)
// sends POST request to /users with the form info provided in HTML form
function loadApp() {
    console.log("Loading application - please wait");

    // Set custom front page content on load
    $("#editableFrontPageContent").load("./modifiableContent/frontPage.html");


    // Login button
    $("#button").click(function (event) {
        event.preventDefault(); /* Prevent default action */
        $.post("/login", $("#login").serializeArray(), function (data) {
            console.log("loadApp() / Login : Sending POST to the server");
        }).done(function (data) {
            if (data.action === "redirect") {
                window.location.replace(data.data);
            } else {
                document.getElementById("loginStatus").innerHTML = '<p style="color:red;margin-top:2%">' + data.data + '</p>';
                //document.getElementById("index_login").style.display = "none";
                //document.getElementById("column_wide").className = "col-lg-12";
            }
            console.log(data);
        }).fail(function (err) {
            console.log("loadApp() / Login Error:\t" + err);
        }).always(function () {
            console.log("loadApp() / Login Finished.");
        });
    });

    // Logout button
    $("#logoutbutton").click(function (event) {
        event.preventDefault(); /* Prevent default action */

        $.get("/logout", function (data) {
            console.log("loadApp() / Logout Sending GET to the server");
        }).done(function (data) {
            document.getElementById("loginStatus").innerText = data.data;
            window.location.reload(false);
        }).fail(function (err) {
            console.log("loadApp() / Logout Error:\t" + err);
        }).always(function () {
            console.log("loadApp() / Logout Finished.");
        });
    });
}

function loadAccount() {
    $.get("/user/get", function (data) {

    }).done(function (data) {
        console.log("loadAccount() / Get User Data: ", data);
        if (data.auth && data.action === "results") {
            $("#inputName").val(data.data.userInfo[0]["name"]);
            $("#inputEmail").val(data.data.userInfo[0]["email"]);
            $("#inputPhoneNumber").val(data.data.userInfo[0]["phoneNumber"]);
        }
    });

    $("#accountModifySubmit").click(function (event) {
        event.preventDefault();
        console.log("click");
        var password = $("#accountModifyForm #inputPassword").val();
        var passwordAgain = $("#accountModifyForm #inputPasswordAgain").val();

        if (password !== "" && password === passwordAgain) {
            $.post("/user/edit", $("#accountModifyForm").serializeArray(), function (data) {
                console.log("Sending account modify POST request to server.");
            }).done(function (data) {
                if (data.auth) {
                    $.get("/logout", function (data) {

                    }).done(function (data) {
                        window.alert("Sinut kirjataan ulos, kirjaudu uudestaan uusilla tiedoillasi");
                        window.location.replace("/");
                    });
                }
                else {
                    document.getElementById("accountModifyStatus").innerText = data.data;
                }
            })
        } else {
            document.getElementById("accountModifyStatus").innerText = "Kirjoita sama uusi salasana molempiin kenttiin.";
        }
    });
}

// loads a list of events that the current user has signed up for
function loadOwnEvents(eventType) {
    $.get("/user/viewSignups", function (data) {
    }).done(async function (data) {
        if (data.auth && data.action === "results") {
            ownSignups = JSON.stringify(data.data); // the ids of events the user has signed up for
            
            // get all events
            $.get("/events/view", function (data) {

                // filter out events that the user has not signed up for
                let newRows = [];
                for (let i = 0; i < data.data.rows.length; i++) {
                    if (ownSignups.includes(data.data.rows[i].id)) {
                        newRows.push(data.data.rows[i]);
                    }
                }
                data.data.rows = newRows;
            }).done(async function (data) {
                // create list of events similiarly to loadEvents()
                console.log(data);
                if (data.auth && data.action === "results") {
                    await generateToPage(data.data.rows, eventType); // Table to page
                }
                else if (data.auth) {
                    document.getElementById("dataview").innerText = data.data;
                }
                else {
                    window.location.replace("/");
                }
            });
        }
    });
}

async function loadEvents(eventType) {
    await $.get("/user/viewSignups", function (data) {

    }).done(function (data) {
        if (data.auth && data.action === "results") {
            console.log("loadOwnEvents() / Load events: ", data.data);
            ownSignups = JSON.stringify(data.data);
        }
    });

    $.get("/events/view", function (data) {
    }).done(async function (data) {
        console.log(data);
        if (data.auth && data.action === "results") {
            await generateToPage(data.data.rows, eventType); // Table to page

            // Refresh page & view opened collapsible
            const viewEventId = urlParams.get('view');
            if (viewEventId !== null) {
                $('#collapse' + viewEventId).collapse('show');
            }
        }
        else if (data.auth) {
            document.getElementById("dataview").innerText = data.data;
        }
        else {
            window.location.replace("/");
        }
    });

    $("#logoutbutton").click(function (event) {
        event.preventDefault();

        $.get("/logout", function (data) {
            console.log("Sending GET to the server");
        }).done(function (data) {
            window.location.replace("/");
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });

}

// registering page
function loadRegister() {
    const inviteId = urlParams.get('inviteId');
    const email = urlParams.get('email');
    if (email !== null && inviteId !== null && inviteId.length === 32) { /* id length is always 32 */
        console.log("Registering page: found invite id in URL:" + inviteId);

        $("#inputInviteId").val(inviteId);
        $("#inputEmail").val(email);
        $("#registerSubmit").click(function (event) {
            event.preventDefault();

            $.post("/users/new", $("#registerForm").serializeArray(), function (data) {

            }).done(function (data) {
                document.getElementById("registerStatus").innerText = data.data;
            });
        });
    }
    else {
        window.alert("Virheellinen rekisteröintilinkki, tarkista rekisteröintilinkkisi tai ota yhteyttä Kulttuurikimpan ylläpitoon.\r\nPaina OK siirtyäksesi takaisin etusivulle.");
        window.location.replace("/");
    }
}

// admin console
function loadAdmin() {

    // content editing submit button AND content fetching submit buttons:
    // jquery inside maintenance_content.js !

    // register new user, requires email to be present in invite table in database
    $("#registerSubmit").click(function (event) {
        event.preventDefault();
        $.post("/users/new", $("#registerNewUser").serializeArray(), function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("adminStatus").innerText = data.data;
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });

    // accept registration from email - "invite"
    $("#sendInvite").click(function (event) {
        event.preventDefault();
        $.post("/users/invite", $("#registerNewUser").serializeArray(), function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("adminStatus").innerText = data.data;
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });

    // create event
    $("#createEvent").click(function (event) {
        event.preventDefault();
        $.post("/event/new", $("#createNewEvent").serializeArray(), function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("adminStatus").innerText = data.data;
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });

    // modify event details
    $("#modifyEventSubmit").click(function (event) {
        console.log("Modify event !");
        event.preventDefault();

        if (isNaN($('#eventID').val())) {
            document.getElementById("adminStatus").innerText = "Syötä id numeromuodossa";
        }
        else {
            $.post("/event/edit", $("#modifyEvent").serializeArray(), function (data) {
                console.log("Sending POST to the server ( Event Edit )");
            }).done(function (data) {
                document.getElementById("adminStatus").innerText = data.data;
            }).fail(function (err) {
                console.log("Error:\t" + err);
            }).always(function () {
                console.log("Finished.");
            });
        }
    });

    // remove user
    $("#removeUserSubmit").click(function (event) {
        event.preventDefault();
        $.post("/users/remove", $("#removeUser").serializeArray(), function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("adminStatus").innerText = data.data;
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });


    $("#logoutbutton").click(function (event) {
        event.preventDefault();
        $.get("/logout", function (data) {
            console.log("Sending GET to the server");
        }).done(function (data) {
            window.location.replace("/");
        }).fail(function (err) {
            console.log("Error:\t" + err);
        }).always(function () {
            console.log("Finished.");
        });
    });
}

function generateTable(data, targetDiv) {
    // Sarakkeiden nimet
    var columns = [];
    for (var i = 0; i < data.length; i++) {
        for (var key in data[i]) {
            if (columns.indexOf(key) === -1 /*&& key !== "id"*/) {
                columns.push(key);
            }
        }
    }
    //console.log(columns); // debug

    // Luo taulu
    var table = document.createElement("table");

    // Luo rivi
    var table_tr = table.insertRow(); // -1

    // Lisää sarakkeet
    for (var i = 0; i < columns.length; i++) {
        var table_th = document.createElement("th");
        table_th.innerHTML = columns[i];
        table_tr.appendChild(table_th);
    }

    //console.log(data); // debug

    // Lisää data riveittäin
    for (var i = 0; i < data.length; i++) {
        table_tr = table.insertRow(); // -1

        for (var j = 0; j < columns.length; j++) {
            var cell = table_tr.insertCell(); // -1
            //console.log(columns[i]);
            cell.innerHTML = data[i][columns[j]];
        }
    }


    // Lopuksi lisää taulu targetdivideriin

    var container = document.getElementById(targetDiv);
    if (container !== null) {
        container.innerHTML = "";
        container.appendChild(table);
    }
    else {
        console.log("Container was null, not generating table");
    }

}

// Data is refined into mineral
async function generateToPage(data, eventType) {
    // To page
    var toP = "";

    // Event details by events
    var row = [];

    // Data that is displayed on the page
    for (var i = 0; i < data.length; i++) { // was data.length

        row = data[i];

        if (!row.expired && row.type == eventType) {
            
            // Signup panel for event
            toP +=
                '<div class="panel-group">' +
                '<div class="card">' +
                '<div class="card-header">' +
                '<h4 class="card-title mb-0">' +
                '<a data-toggle="collapse" href="#collapse' + i + '">' + ((row.signupAmount >= row.numberOfVolunteers) ? '<img class="eventStatusImg" alt="Täynnä" src="../img/red_circle.png"/>' : '<img class="eventStatusImg" alt="Paikkoja vapaana" src="../img/green_circle.png"/>') + await parseDate(row.date) + ': ' + row.name + '</a>' +
                '</h4>' +
                '</div>' +
                '<div id="collapse' + i + '" class="panel-collapse collapse">' +
                '<div class="card-body" id="card-body' + i + '">' + row.eventText + '<br><br>Ilmoittautuneiden määrä: ' + row.signupAmount + '/' + row.numberOfVolunteers + '<br><br>' +
                '<input type="hidden" id="hiddenEventID' + i + '" value="' + row.id + '"></input>' +
                '<input type="hidden" id="hiddenEventName' + i + '" value="' + row.name + '"></input>'
            var regex = new RegExp(row.id, "g");
            if (ownSignups.match(regex) != null) {
                toP +=
                    '<br><input class="btn btn-secondary my-2 my-sm-0" type="submit" id="deleteSignupButton' + i + '" onClick="deleteButtonClicked(this);" value="Poista ilmoittautumisesi"/>';
            } else if (row.signupAmount < row.numberOfVolunteers) {
                toP +=
                    '<input type="checkbox" id="check' + i + '" onClick="checkboxClicked(this);">Osallistun!</input>' +
                    '<br><textarea id="text' + i + '" placeholder="Lisätiedot" cols="45" rows="1"></textarea>' +
                    '<br><input class="btn btn-secondary my-2 my-sm-0 disabled" type="submit" id="eventSignupButton' + i + '" onClick="sendButtonClicked(this);" value="Lähetä"/>';
            }
            toP +=
                '</div>' +
                '</div>' +
                '</div>';
        }
    }

    //console.log('Rows:', rows);
    // Data to page
    document.getElementById('accordionExample').innerHTML = toP;
}

// Convert YYYY-MM-DD dates to DD.MM.YYYY
function parseDate(date) {
    return new Promise((resolve, reject) => {
        try {
            let splitDate = date.split("-", 3);
            let fixedDate = splitDate[2] + "." + splitDate[1] + "." + splitDate[0];
            resolve(fixedDate);
        }
        catch (err) {
            console.log("Error parsing date, returning in default date format. (" + err + ")");
            resolve(date);
        }
    });
}

function checkboxClicked(element) {
    if (element.checked) {
        $("#eventSignupButton" + element.id.split("check")[1]).removeClass("disabled");
    } else {
        $("#eventSignupButton" + element.id.split("check")[1]).addClass("disabled");
    }
}

function sendButtonClicked(element) {
    var i = element.id.split("eventSignupButton")[1];
    if (document.getElementById("check" + i).checked) {
        document.getElementById("eventID").value = document.getElementById("hiddenEventID" + i).value;
        document.getElementById("userMessage").value = document.getElementById("text" + i).value;
        var eventName = document.getElementById("hiddenEventName" + i).value;
        $.post("/event/signup", $("#eventSignup").serializeArray(), function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("eventSignupStatus").innerText += '\n' + 'Tapahtuman: "' + eventName + '". ' + data.data;
            console.log(data.data);
            if (data.auth && i !== null && window.location.pathname === "/kimppatapahtumat/") {
                window.location.replace("/kimppatapahtumat?view=" + i); // Refresh & open currently viewed event
            }
            else if (data.auth && i !== null && window.location.pathname === "/kamutapahtumat/") {
                window.location.replace("/kamutapahtumat?view=" + i); // Refresh & open currently viewed event
            }
            else if (data.auth && i !== null && window.location.pathname === "/omatkimppatapahtumat/") {
                window.location.replace("/omatkimppatapahtumat");
            }
            else if (data.auth && i !== null && window.location.pathname === "/omatkamutapahtumat/") {
                window.location.replace("/omatkamutapahtumat");
            }
        });
    }
}

function deleteButtonClicked(element) {
    var i = element.id.split("deleteSignupButton")[1];
    document.getElementById("eventID").value = document.getElementById("hiddenEventID" + i).value;
    var eventName = document.getElementById("hiddenEventName" + i).value;
    $.post("/event/signup/delete", $("#eventSignup").serializeArray(), function (data) {
        console.log("Sending POST to the server");
    }).done(function (data) {
        document.getElementById("eventSignupStatus").innerText += '\n' + 'Tapahtuman: "' + eventName + '". ' + data.data;
        console.log(data.data);
        if (data.auth && i !== null && window.location.pathname === "/kimppatapahtumat/") {
            window.location.replace("/kimppatapahtumat?view=" + i); // Refresh & open currently viewed event
        }
        else if (data.auth && i !== null && window.location.pathname === "/kamutapahtumat/") {
            window.location.replace("/kamutapahtumat?view=" + i); // Refresh & open currently viewed event
        }
        else if (data.auth && i !== null && window.location.pathname === "/omatkimppatapahtumat/") {
            window.location.replace("/omatkimppatapahtumat");
        }
        else if (data.auth && i !== null && window.location.pathname === "/omatkamutapahtumat/") {
            window.location.replace("/omatkamutapahtumat");
        }
    });
}