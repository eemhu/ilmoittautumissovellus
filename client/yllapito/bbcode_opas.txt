Kulttuurikimppa Ylläpito
BBCode opas

Voit käyttää BBCodea tekstien muotoiluun valitsemalla editorin oikeasta yläkulmasta "BBCODE" -vaihtoehdon.

Tämän jälkeen voit syöttää tekstiä erilaisten tagien sisään:
[lihavoitu]Teksti tähän väliin[/lihavoitu]
[kursivoitu]Teksti tähän väliin[/kursivoitu]
[alleviivattu]Teksti tähän väliin[/alleviivattu]
[otsikko]Teksti tähän väliin[/otsikko]

Voit myös ottaa BBCODE -tilan pois käytöstä, ja maalata tekstin ja valita haluamasi vaihtoehto, käyttämättä BBCODEa.
