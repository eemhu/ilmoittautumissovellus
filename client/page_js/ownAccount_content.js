// ownAccount page content
function ownAccountContent() {
    if (document.getElementById('ownAccount_content') != null) {
        document.getElementById('ownAccount_content').innerHTML =
        '<div class="container" id="accountPageContainer">' +
        '<form class="form" id="accountModifyForm">' +

        '<h1 class="h3 mb-3 font-weight-normal">Käyttäjän tietojen muuttaminen</h1>' +

        '<label for="inputName">Nimi</label>' +
        '<input type="text" id="inputName" name="name" class="form-control" placeholder="Nimi" required autofocus>' +

        '<label for="inputEmail">Sähköposti</label>' +
        '<input type="text" id="inputEmail" name="email" class="form-control" placeholder="Sähköposti" required autofocus>' +

        '<label for="inputPhoneNumber">Puhelinnumero</label>' +
        '<input type="text" id="inputPhoneNumber" name="phoneNumber" class="form-control" placeholder="Puhelinnumero" required autofocus>' +

        '<label for="inputPassword">Uusi salasana</label>' +
        '<input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Uusi salasana (vähintään 6 merkkiä)" required>' +

        '<label for="inputPasswordAgain">Uusi salasana uudestaan</label>' +
        '<input type="password" id="inputPasswordAgain" name="passAgain" class="form-control" placeholder="Uusi salasana uudestaan" required>' +

        '<label for="inputOldPassword">Nykyinen salasana</label>' +
        '<input type="password" id="inputOldPassword" name="oldPass" class="form-control" placeholder="Nykyinen salasana" required>' +

        '<button class="btn btn-lg btn-primary btn-block" type="submit" id="accountModifySubmit">Toteuta muutokset</button>' +

        '<div id="accountModifyStatus"></div>' +

        '</form>' +
        '</div>';
    }
}


/* 

'<div class="container">' +
        '<h2>Käyttäjän tiedot</h2><br>' +
        '<div id="name"></div>' +
        '<div id="email"></div>' +
        '<div id="phoneNumber"></div>' +
    '</div>';

*/