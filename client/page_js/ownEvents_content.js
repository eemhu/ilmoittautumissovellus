//Index page content
function ownEventsContent(header){
    if(document.getElementById('ownEvents_content') != null){
        document.getElementById('ownEvents_content').innerHTML =
            '<div class="container">' +
                '<h2>' + header + '</h2><br>' +
                '<p>Tällä sivulla listataan kaikki kimpat, joihin olet ilmoittautunut. Voit poistaa ilmoittautumisesi kätevästi tällä sivulla. Jos sivu on tyhjä, et ole ilmoittautunut yhteenkään kimppaan tai kimpat ovat jo vanhentuneet.</p><br>' +
                '<div class="accordion" id="accordionExample">' + '</div>' +
                '<form id="eventSignup">' +
                    '<input type="text" name="eventID" id="eventID" style="display:none"/>' +
                    '<input type="text" name="userMessage" id="userMessage" style="display:none"/>' +
                '</form>' +
                '<div id="eventSignupStatus"></div>' +
                '<br>' +
            '</div>';
    }
}