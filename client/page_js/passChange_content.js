// PassChange page
async function passChangeContent() {

    //Parameter check first
    //If paramaters not found -> load page for passchange
    if (document.getElementById("passChange_content") != null && await checkLink() === false) {
        document.getElementById("passChange_content").innerHTML = 
        '<div class="container" id="passChangeContainer">' +
            '<form>' +
                '<h1 class="h3 mb-3 font-weight-normal">Tilin salasanan vaihtaminen</h1>' +

                '<p>Kirjoita sähköpostiosoitteesi alla olevaan kenttään niin lähetämme sinulle viestin, jonka avulla voit asettaa uuden salasanan.</p>' +

                '<label for="inputEmail">Sähköposti</label>' +
                '<input type="text" id="inputEmail" name="email" class="form-control" placeholder="Sähköposti" required autofocus>' +

                '<button class="btn btn-lg btn-primary btn-block" type="submit" id="changeSubmit">Lähetä</button>' +
            '</form>' +
        '</div>';
    }
}

// When link has correct changeId & email combo
function changeInputsToPage(){
    document.getElementById("passChange_content").innerHTML = 
    '<div class="container" id="passChangeContainer">' +
        '<form onsubmit="done()">' +
            '<h1 class="h3 mb-3 font-weight-normal">Tilin salasanan vaihtaminen</h1>' +

            '<p>Kirjoita haluamasi uusi salasana alla oleviin kenttiin. Salasanan minimipituus on 6 merkkiä.</p>' +

            '<label for="inputPass1">Salasana</label>' +
            '<input pattern=".{6,}"  type="password" id="inputPass1" name="pass1" class="form-control" placeholder="Salasana (minimipituus 6 merkkiä)" required autofocus>' +

            '<input pattern=".{6,}" type="password" id="inputPass2" name="pass2" class="form-control" placeholder="Salasana (uudestaan)" required>' +

            '<button onclick="comparePass()" class="btn btn-lg btn-primary btn-block" id="changeSubmit">Lähetä</button>' +

        '</form>' +
    '</div>';
}

// Compare password inputs from user and if correct -> change password
function comparePass(){
    var val1 = document.getElementById('inputPass1').value,
        val2 = document.getElementById('inputPass2').value,
        email = urlParams.get('email');

    if(val1 != val2){
        document.getElementById('inputPass1').setCustomValidity("Salasanat eivät ole samat tai salasanan pituus on alle 6 merkkiä");
    }else if (val1.length >= 6){
        //document.getElementById('inputPass1').setCustomValidity("");
        $.post("/users/changePass", {email: email, pass: val1}, function (data) {
            console.log("Sending POST to server");
        }).done(function (data) {
            console.log("DATA", data.data);
            window.alert(data.data);
            window.location.replace("/");
        });
    }
}
function done(){
    window.alert("Salasanan vaihto on käynnissä");
    window.location.replace("/");
}


// Check if link has parmeters for password change and whether they are proper if they exist
async function checkLink() {
    const changeId = urlParams.get('changeId');
    const email = urlParams.get('email');

    // User requested passchange
    if(email !== null && changeId === null){
        addPermission();
    }

    if (email !== null && changeId !== null) {
        // Check email and changeId
        var isOK = await checkPair(email, changeId);

        // ^If true -> change page for passChange
        if(isOK.data === 1){
            changeInputsToPage();
        }else{
            window.alert("Virheellinen salasanan vaihtolinkki, tarkista linkkisi tai ota yhteyttä Kulttuurikimpan ylläpitoon.\r\nPaina OK siirtyäksesi takaisin etusivulle.");
            window.location.replace("/");
        }

        return true;
    }
    return false;
}

// Add permission for password change to database
function addPermission() {
    var email = urlParams.get('email');
    console.log("pog");

    $.post("/users/passChangePermission", {email: email}, function (data) {
        console.log("Sendin GET to server");
    }).done(function (data) {
        console.log("DATA", data.data);
        window.alert(data.data);
        window.location.replace("/");
    });
}

// Check from database if email and changeId pairs exists
function checkPair(email, changeId){
    console.log("changeId: " + changeId + " email: "+ email);
    return $.post("/users/passCheckPair", {email: email, changeId: changeId}, function (data) {
        console.log("Sendin POST to server");
    }).done(function (data) {
        console.log("DATA", data.data);
        return data.data;
    });
}