//Index page content
function indexContent(){
 
    if(document.getElementById('index_content') != null){
    document.getElementById('index_content').innerHTML =

        '<div class="container" id="wider_container">' +
            '<div class="row" id="index_info">' +
                '<div class="col-lg-7" id="column_wide">' +
                    '<p><div class="mw-50 mh-50" id="editableFrontPageContent"></div></p>' +
                '</div>' +
                '<div class="col-lg-5 justify-content-center" id="index_login">' +
                   '<form class="form-signin text-center d-none" id="login">' +
                        '<img class="mb-4" src="img/kimppalogo.png" alt="" width="72" height="72">' +
                        '<h1 class="h3 mb-3 font-weight-normal">Kirjautuminen</h1>' +
                        '<label for="inputEmail" class="sr-only">Sähköposti</label>' +
                        '<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Sähköposti" required autofocus>' +
                        '<label for="inputPassword" class="sr-only">Salasana</label>' +
                        '<input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Salasana" required>' +
                        '<button class="btn btn-lg btn-primary btn-block" type="submit" id="button">Kirjaudu sisään</button>' +
                        '<div id="loginStatus"></div>' +
                        '<a href="../passChange">Unohditko salasanan?</a>' +
                        '<p class="text-muted">&copy; 2019</p>' +
                    '</form>'+
                    //Login end
                '</div>' +
            '</div>' +
            '<hr>'
        '</div>';
    }
}