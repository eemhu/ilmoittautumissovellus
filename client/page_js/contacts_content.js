//Contacts page content
function contactsContent() {
 
    if (document.getElementById('contacts_content') != null) {
        document.getElementById('contacts_content').innerHTML =
            '<div class="container" id="wider_container">' +
                '<div class="row" id="index_info">' +
                    '<div class="col-lg-12" id="column_wide_contacts">' +
                        '<h2>Yhteystiedot</h2>' +
                        '<p><div class="mw-50 mh-50" id="editableContactsPageContent">Odota hetki, ladataan tietoja</div></p>' +
                    '</div>' +
                '</div>' +
                '<hr>'
            '</div>';
    }
}