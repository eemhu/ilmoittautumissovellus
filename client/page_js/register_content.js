// Registering page content (displayed to user)
function registerContent() {
    if (document.getElementById("register_content") != null) {
        document.getElementById("register_content").innerHTML = 
        '<div class="container" id="registerPageContainer">' +
        '<form class="form" id="registerForm">' +

        '<h1 class="h3 mb-3 font-weight-normal">Kulttuurikimppaan rekisteröinti</h1>' +

        '<label for="inputName">Nimi</label>' +
        '<input type="text" id="inputName" name="name" class="form-control" placeholder="Nimi" required autofocus>' +

        '<label for="inputEmail">Sähköposti</label>' +
        '<input type="text" id="inputEmail" name="email" readonly class="form-control" required>' +

        '<label for="inputPhoneNumber">Puhelinnumero</label>' +
        '<input type="text" id="inputPhoneNumber" name="phoneNumber" class="form-control" placeholder="Puhelinnumero" required>' +

        '<label for="inputPassword">Salasana</label>' +
        '<input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Salasana (vähintään 6 merkkiä)" required>' +

        '<label for="inputPasswordAgain">Salasana uudestaan</label>' +
        '<input type="password" id="inputPasswordAgain" name="passAgain" class="form-control" placeholder="Salasana uudestaan" required>' +

        '<label for="inputInviteId">Kutsun tunniste</label>' +
        '<input type="text" id="inputInviteId" name="inviteId" readonly class="form-control" required>' +

        '<button class="btn btn-lg btn-primary btn-block" type="submit" id="registerSubmit">Rekisteröidy</button>' +

        '<div id="registerStatus"></div>' +

        '</form>' +
        '</div>';
    }
}