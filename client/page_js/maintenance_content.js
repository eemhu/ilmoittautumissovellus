let eventData;

var tabs = ["#eventTab", "#userTab", "#frontPageEditorTab", "#contactsEditorTab", "#page_1EditorTab", "#page_2EditorTab"];
var currentTab = undefined;

async function maintenanceContent() {
    if (document.getElementById('maintenance_content') != null && await returnAdminAuthState()) {
        document.getElementById('maintenance_content').innerHTML =
            '<div class="container" id="temp">' +
                '<div id="maintenanceContainer">' +
                    '<h2>Ylläpito</h2>' +
                    '<ul class="nav nav-tabs">' +
                    '<li class="nav-item">' +
                    '<a class="nav-link active" id="eventTab" onclick="switchToEventView();" href="#">Tapahtumat</a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" id="userTab" onclick="switchToUserView();" href="#">Käyttäjät</a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" id="frontPageEditorTab" onclick="switchToFrontPageEditorView();" href="#">Muokkaa etusivua</a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" id="contactsEditorTab" onclick="switchToContactsEditorView();" href="#">Muokkaa yhteystietoja</a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" id="page_1EditorTab" onclick="switchToPage_1EditorView();" href="#">Muokkaa käsikirjaa</a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" id="page_2EditorTab" onclick="switchToPage_2EditorView();" href="#">Muokkaa ajankohtaista-osiota</a>' +
                    '</li>' +
                    '</ul>' +
                    '<div id="contentView">' +
                    '</div>' +               
                '</div>' +

            '<div id="modalView" class="modal fade" role="dialog">' +
            '</div>'+
            '</div>';
        switchToEventView(); // go to default view
        //loadAdmin(); // load admin
    }
    else {
        window.location.replace("/");
    }
}

var persistentEventData = [];
async function switchToEventView() {
    let view = document.getElementById('contentView');

    if (view != null) {

        let userPage = '<br><div class="container" id="eventPageContainer">' +
                        '<form class="form" id="eventForm">' +

                        '<h1 class="h3 mb-3 font-weight-normal">Tapahtuman lisääminen</h1>' +
                
                        '<label for="inputEventName">Nimi</label>' +
                        '<input type="text" id="inputEventName" name="eventName" class="form-control" placeholder="Nimi" required autofocus>' +
                
                        '<label for="inputEventText">Infoteksti</label>' +
                        '<textarea class="form-control" id="inputEventText" name="eventText" class="form-control" rows="3" placeholder="Infoteksti" required autofocus></textarea>' +
                
                        '<label for="inputNumberOfVolunteers">Vapaaehtoisten lukumäärä</label>' +
                        '<input type="number" id="inputNumberOfVolunteers" name="numberOfVolunteers" min="1" max="9999" value="1" class="form-control" placeholder="Vapaaehtoisten lukumäärä" required autofocus>' +
                
                        '<label for="inputEventDate">Päivämäärä</label>' +
                        '<input type="date" id="inputEventDate" name="eventDate" class="form-control" placeholder="Päivämäärä" required>' +

                        '<label class="form-check form-check-inline">Valitse tapahtuman tyyppi:</label>' +
                        '<div class="form-check form-check-inline">' +
                        '<input class="form-check-input" type="radio" name="eventType" id="eventTypeRadio1" value="1" required checked>' +
                        '<label class="form-check-label" for="eventTypeRadio1">Kimppatapahtuma</label>' +
                        '</div>' +
            
                        '<div class="form-check form-check-inline">' +
                        '<input class="form-check-input" type="radio" name="eventType" id="eventTypeRadio1" value="2" required>' +
                        '<label class="form-check-label" for="eventTypeRadio1">Kamutapahtuma</label>' +
                        '</div>' +
                
                        '<button class="btn btn-lg btn-primary btn-block" type="submit" id="addEventSubmit">Lisää tapahtuma</button>' +
                
                        '<div id="eventSubmitStatus"></div>' +
                
                        '</form>';

        await $.get("/events/view", function (data) {
        }).done(async function (data) {
            if (data.auth && data.action === "results") {
                eventData = data.data.rows; // store event data for signup view
                userPage +=
                    '<br><h1 class="h3 mb-3 font-weight-normal">Tapahtumalista</h1>' +
                    '<br><div class="table-responsive"><table class="table"><thead><tr>' +
                    '<th>Nimi</th>' +
                    '<th>Teksti</th>' +
                    '<th>Osallistujat</th>' +
                    '<th>Päivämäärä</th>' +
                    '<th>Voimassa</th>' +
                    '<th>Tyyppi</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';
                data.data.rows.forEach(function (event) {
                    persistentEventData[event.id] = event;
                    //console.log(event);
                    let splitDate, parsedDate;
                    let eventType;

                    if (event.type !== null && event.type === 1) {
                        eventType = "Kimppatapahtuma";
                    }
                    else if (event.type !== null && event.type === 2) {
                        eventType = "Kamutapahtuma";
                    }
                    else {
                        eventType = "???";
                    }

                    try {
                        splitDate = event.date.split("-", 3);
                        parsedDate = String.prototype.concat(splitDate[2],".",splitDate[1],".",splitDate[0]);
                    }
                    catch (err) {
                        console.log("error parsing date (Using non-parsed date as failsafe): %s", err.message);
                        parsedDate = event.date;
                    }
                    userPage +=
                        '<tr>' +
                        '<td>' + event.name + '</td>' +
                        '<td>' + event.eventText + '</td>' +
                        '<td>' + event.signupAmount + " / " + event.numberOfVolunteers + '</td>' +
                        '<td>' + parsedDate + '</td>' +
                        '<td>' + ((event.expired) ? 'Ei' : 'Kyllä') + '</td>' +
                        '<td>' + eventType + '</td>' +
                        '<td><div class="dropdown" id="eventActionsDropDown">' +
                        '<button class="btn btn-secondary dropdown-toggle" type="button" id="eventDropDownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                        'Toiminnot' +
                        '</button>' +
                        '<div class="dropdown-menu" aria-labelledby="eventDropDownMenuButton">' +
                        ((event.expired) ? '<input class="dropdown-item" href="#" value="Ei voi muokata" disabled/>' : '<input class="dropdown-item" href="#" type="submit" id="editEventButton-' + event.id + '" onClick="editEventButtonClicked(this);" value="Muokkaa"/>') +
                        '<input class="dropdown-item" href="#" type="submit" id="deleteEventButton-' + event.id + '" onClick="deleteEventButtonClicked(this);" value="Poista"/>' +
                        '<input class="dropdown-item" href="#" type="submit" id="showSignupsButton-' + event.id + '" onClick="showSignupsButtonClicked(this);" value="Ilmoittautuneet"/>' +
                        '</div>' +
                        '</div></td>' +
                        '</tr>';
                });
                userPage += '</tbody></table></div></div>';
            }
            else if (data.auth) {
                document.getElementById("dataview").innerText = data.data;
            }
            else {
                window.location.replace("/");
            }
        });
        
        view.innerHTML = userPage;


        $("#addEventSubmit").click(function (event) {
            event.preventDefault();
            $.post("/event/new", $("#eventForm").serializeArray(), function (data) {
                console.log("send event add POST to server");
            }).done(function (data) {
                if (data.auth && data.action === "success") {
                    window.alert(data.data);
                    switchToEventView(); // refresh
                }
                else if (data.auth && (data.action === "missingInfo" || data.action === "invalidInfo")) {
                    document.getElementById("eventSubmitStatus").innerText = data.data;
                }
                else {
                    window.alert(data.data + "\r\nPaina OK siirtyäksesi etusivulle.");
                    window.location.replace("/");
                }
            });
        });
        
        currentTab = "#eventTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });
    }
}

function editEventButtonClicked(element) {
    console.log("muokkaa tapahtuma");

    // retrieve event id
    var eventID = element.id.split("-")[1];

    // modal div
    let modal = document.getElementById("modalView");
    if (modal != null) {
        modal.innerHTML = '<div class="modal-dialog" role="document">' +
                            '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                '<h5 class="modal-title">Muokkaa tapahtumaa</h5>' +
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Peruuta">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                '</div>' +
                                '<div id="editingModalBody" class="modal-body">' +
                                
                                '<form class="form" id="eventEditForm">' +
                        
                                '<label for="inputEventName">Nimi</label>' +
                                '<input type="text" id="inputEventName" name="eventName" class="form-control" placeholder="Nimi" required autofocus>' +
                        
                                '<label for="inputEventText">Infoteksti</label>' +
                                '<textarea class="form-control" id="inputEventText" name="eventText" class="form-control" rows="3" placeholder="Infoteksti" required autofocus></textarea>' +
                        
                                '<label for="inputNumberOfVolunteers">Vapaaehtoisten lukumäärä</label>' +
                                '<input type="number" id="inputNumberOfVolunteers" name="numberOfVolunteers" min="1" max="9999" value="1" class="form-control" placeholder="Vapaaehtoisten lukumäärä" required autofocus>' +
                        
                                '<label for="inputEventDate">Päivämäärä</label>' +
                                '<input type="date" id="inputEventDate" name="eventDate" class="form-control" placeholder="Päivämäärä" required>' +

                                '<label for="inputEventID">EventID (dev only)</label>' +
                                '<input type="text" id="inputEventID" name="eventID" class="form-control" placeholder="Tapahtuma id" readonly required>' + /* TODO: change to hidden when works */
                                                
                                '<div id="eventEditStatus"></div>' +
                        
                                '</div>' +
                                '<div class="modal-footer">' +
                                '<button type="submit" id="editEventSubmit" class="btn btn-primary">Tallenna muutokset</button>' +
                                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Peruuta</button>' +
                                '</form>' +
                                '</div>' +
                            '</div>' +
                            '</div>';

        // display modal
        $("#modalView").modal('show');

        // fetch event data from persistentEventData array
        $("#eventEditForm #inputEventName").val(persistentEventData[eventID]["name"]);
        $("#eventEditForm #inputEventText").val(persistentEventData[eventID]["eventText"]);
        $("#eventEditForm #inputNumberOfVolunteers").val(persistentEventData[eventID]["numberOfVolunteers"]);
        $("#eventEditForm #inputEventDate").val(persistentEventData[eventID]["date"]);
        $("#eventEditForm #inputEventID").val(eventID);

        // current signup amount equals minimum
        let signupAmount = persistentEventData[eventID]["signupAmount"];
        if (signupAmount !== 0) {
            $("#eventEditForm #inputNumberOfVolunteers").attr("min", signupAmount);
        }
        else {
            $("#eventEditForm #inputNumberOfVolunteers").attr("min", 1);
        }
        

        // bind listener to submit button
        $("#editEventSubmit").click(function (event) {
            event.preventDefault();

            let volunteerAmountCheck = $("#eventEditForm #inputNumberOfVolunteers").val() >= persistentEventData[eventID]["signupAmount"];
            if (volunteerAmountCheck) {
                $.post("/event/edit", $("#eventEditForm").serializeArray(), function (data) {
                    console.log("Sending event edit POST to server");
                }).done(function (data) {
                        if (data.auth && data.action === "message") {
                            $("#modalView").modal('hide');
                            switchToEventView(); //refresh
                        }
                        else {
                            window.alert(data.data);
                        }
                        
                });
            }
            else {
                window.alert("Virheellinen määrä vapaaehtoisia");
            }
        });
    }
}

function deleteEventButtonClicked(element) {
    if (confirm("Oletko aivan varma, että haluat poistaa valitun tapahtuman? Toimintoa ei voi peruuttaa.")) {
        console.log("poista tapahtuma");
        var eventID = element.id.split("-")[1];
        $.post("/event/remove", { eventID : eventID }, function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            if (data.auth) {
                window.alert(data.data);
                console.log(data.data);
                switchToEventView(); // refresh page after success
            }
            else {
                window.alert(data.data + "\r\nPaina OK siirtyäksesi etusivulle.");
                window.location.replace("/");
            }
        });
    }
    else {
        window.alert("Toiminto peruutettiin.");
    }
}

// When clicking the delete button of a signup, remove the signup from the database.
function deleteSignupButtonClicked(element) {
    if (confirm("Oletko aivan varma, että haluat poistaa valitun ilmoittautumisen? Toimintoa ei voi peruuttaa.")) {
        var signupID = element.id.split("-")[1];
        $.post("/signups/delete", { signupID : signupID }, function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            if (data.auth) {
                window.alert(data.data);
                console.log(data.data);
                $('#modalView').modal('hide'); // hide modal after success
            }
            else {
                window.alert(data.data);
            }
        });
    }
    else {
        window.alert("Toiminto peruutettiin.");
    }
}

// When the showSignupsButton of an event is clicked, show a modal with signup information.
async function showSignupsButtonClicked(element) {
    let modal = document.getElementById('modalView');
    var eventID = element.id.split("-")[1];

    let signupInfo = await getEventSignupInfo(eventID);
    let signupTable = await createSignupTable(signupInfo);

    if(signupInfo.length === 0) {
        window.alert("Ei löytynyt ilmoittautumisia.");
        return;
    }

    modal.innerHTML =
        '<div class ="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<h4 class="modal-title">' + signupInfo[0].eventName + '\r\nIlmoittautumiset</h4>' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                '</div>' +
                '<div class="modal-body">' +
                    signupTable +
                '</div>' +
            '</div>' +
        '</div>';

    $('#modalView').modal('show');
}

// Creates a table of signups with additional information (the name and email of a user and the event name)
async function getEventSignupInfo(eventID) {
    // get all signups and store in a variable
    let signups;
    await $.get("/signups/get", function (data) {
    }).done(async function (data) {
        if (data.auth && data.action === "results") {
            signups = data.data.rows;
        }
        else {
            window.location.replace("/");
        }
    });

    // store all events in a variable
    let events = eventData;
    
    // get all users and store in a variable
    let users;
    await $.get("/users/get", function (data) {
    }).done(async function (data) {
        if (data.auth && data.action === "results") {
            users = data.data.rows;
        }
        else {
            window.location.replace("/");
        }
    });

    // create a new array with signup info plus users' names and emails as well as event names
    let signupsWithInfo = [];
    let event = events.find(event => event.id == eventID);
    if (event == undefined) {
        console.log("Error retrieving signup information: event undefined.");
        console.log(event);
        return;
    }

    let j = 0; // index for signupsWithInfo
    for (let i = 0; i < signups.length; i++) {
        if (signups[i].eventID == eventID) {
            let user = users.find(user => user.id === signups[i].userID);
    
            if (user == undefined) {
                console.log("Error retrieving signup information: user undefined.");
                continue;
            }
    
            signupsWithInfo[j] = signups[i];
            signupsWithInfo[j].userName = user.name;
            signupsWithInfo[j].userEmail = user.email;
            signupsWithInfo[j].eventName = event.name;
            j++;
        }
    }

    return signupsWithInfo;
}

// Based on the output of getEventSignupInfo, create a table of signups to be displayed in a modal.
// Each signup/row also has a delete button.
async function createSignupTable(signupInfo) {
    let table = 
        '<div class="table-responsive"><table class="table"><thead><tr>' +
        '<th>Nimi</th>' +
        '<th>Sähköpostiosoite</th>' +
        '<th>Viesti</th>' +
        '<th></th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

    signupInfo.forEach(function (signup) {
        table +=
            '<tr>' +
            '<td>' + signup.userName + '</td>' +
            '<td>' + signup.userEmail + '</td>';
        if (signup.userMessage) {
            table += '<td>' + signup.userMessage + '</td>';
        } else {
            table += '<td></td>';
        }
        table +=
            '<td><input class="btn btn-secondary my-2 my-sm-0" type="submit" id="deleteSignupButton-' + signup.id + '" onClick="deleteSignupButtonClicked(this);" value="Poista"/></td>' +
            '</tr>';
    });

    table += '</tbody></table></div></div>';

    return table;
}

async function switchToUserView() {
    let view = document.getElementById('contentView');

    if (view != null) {
        let userPage = '<br><div class="container" id="invitePageContainer">' +
            '<form class="form" id="inviteForm">' +

            '<h1 class="h3 mb-3 font-weight-normal">Kutsun lähettäminen</h1>' +

            '<label for="inputEmail">Sähköposti</label>' +
            '<input type="text" id="inputEmail" name="email" class="form-control" placeholder="Sähköposti" required autofocus>' +

            '<label class="form-check form-check-inline">Valitse käyttäjän tyyppi:</label>' +
            '<div class="form-check form-check-inline">' +
            '<input class="form-check-input" type="radio" name="isAdmin" id="isAdminRadio1" value="0" required checked>' +
            '<label class="form-check-label" for="isAdminRadio1">Kamu</label>' +
            '</div>' +

            '<div class="form-check form-check-inline">' +
            '<input class="form-check-input" type="radio" name="isAdmin" id="isAdminRadio2" value="1" required>' +
            '<label class="form-check-label" for="isAdminRadio2">Ylläpitäjä</label>' +
            '</div>' +

            '<button class="btn btn-lg btn-primary btn-block" type="submit" id="inviteSubmit">Lähetä kutsu</button>' +

            '<div id="inviteStatus"></div>' +

            '</form>';

        await $.get("/users/get", function (data) {
        }).done(async function (data) {
            if (data.auth && data.action === "results") {
                userPage +=
                    '<br><h1 class="h3 mb-3 font-weight-normal">Käyttäjälista</h1>' +
                    '<br><div class="table-responsive"><table class="table"><thead><tr>' +
                    '<th>Nimi</th>' +
                    '<th>Sähköposti</th>' +
                    '<th>Puhelinnumero</th>' +
                    '<th>Käyttäjän tyyppi</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';
                data.data.rows.forEach(function (user) {
                    let currentUserType = (user.admin ? "Ylläpitäjä" : "Kamu");
                    let currentUserRemoveButton = (user.permanent ? '<p>Pysyvä tili, ei voi poistaa</p>' : '<input class="btn btn-secondary my-2 my-sm-0" type="submit" id="deleteUserButton-' + user.email + '" onClick="deleteUserButtonClicked(this);" value="Poista käyttäjä"/>');
                    //if (!user.admin) {
                        userPage +=
                            '<tr>' +
                            '<td>' + user.name + '</td>' +
                            '<td>' + user.email + '</td>' +
                            '<td>' + user.phoneNumber + '</td>' +
                            '<td>' + currentUserType + '</td>' +
                            '<td>' + currentUserRemoveButton + '</td>' +
                            '</tr>';
                    //}
                });
                userPage += '</tbody></table></div></div>';
            }
            else if (data.auth) {
                document.getElementById("dataview").innerText = data.data;
            }
            else {
                window.location.replace("/");
            }
        });
        view.innerHTML = userPage;
        
        // switch tab style to active, deactivate others
        currentTab = "#userTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });

        $("#inviteSubmit").click(function (event) {
            console.log("painettu");
            event.preventDefault();
            if (confirm("Oletko aivan varma, että haluat lähettää kutsun sähköpostiosoitteeseen " + $("#inputEmail").val() + " ?")) {
                $.post("/users/invite", $("#inviteForm").serializeArray(), function (data) {

                }).done(function (data) {
                    document.getElementById("inviteStatus").innerText = data.data;
                });
            }
            else {
                window.alert("Kutsun lähetys peruutettiin.");
            }  
        });
    }
}

function deleteUserButtonClicked(element) {
    var userEmail = element.id.split("-")[1];
    if (confirm("Oletko aivan varma, että haluat poistaa valitun (" + userEmail + ") kamun? Toimintoa ei voi peruuttaa, eikä tietoja voi palauttaa. Painamalla OK poistat kamun ja sen tiedot pysyvästi.")) {
        $.post("/users/remove", { email: userEmail }, function (data) {
            console.log("Sending POST to the server");
        }).done(function (data) {
            document.getElementById("inviteStatus").innerText = data.data;
            console.log(data.data);
            switchToUserView(); // refresh page after success
        });
    }
    else {
        window.alert("Peruutit kamun poiston. Kamua ja sen tietoja ei siis poistettu.");
    }
    
}

function switchToFrontPageEditorView() {
    let view = document.getElementById('contentView');

    if (view != null) {
        view.innerHTML = '<textarea id="editor"></textarea><br>' +
            '<form id="editForm">' +
            '<p><b>Huom!</b> Saat parhaimman lopputuloksen, jos kirjoitat tekstin ensin ja sen jälkeen lisäät muotoilut.</p>' +
            '<button class="btn btn-secondary" type="submit" id="submitFrontPageEdit">Tallenna muutokset</button>' +
            '<div id="editStatus"></div>' +
            '</form>';

        // switch tab style to active, deactivate others
        currentTab = "#frontPageEditorTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });

        $("#submitFrontPageEdit").click(function (event) {
            console.log("Edit submit click");
            event.preventDefault();
            if (confirm("Oletko aivan varma, että haluat tallentaa tiedot etusivulle? Toimintoa ei voi peruuttaa.")) {
                $.post("/content/edit", { edit: "frontPage", html: $("#editor").htmlcode() }, function (data) {

                }).done(function (data) {
                    if (data.auth && data.action === "message" || data.auth && data.action === "error") {
                        //document.getElementById("editStatus").innerText = data.data;
                        window.alert(data.data);
                    }
                    else {
                        window.alert(data.data);
                        window.location.replace("/");
                    }
                });
            }
            else {
                window.alert("Toiminto peruutettiin, ja tietoja ei tallennettu.");
            }

        });

        // enable wysibb editor
        $("#editor").wysibb(wysibbOptions);

        // get old editable page content
        $.get("/content/get", { type: "frontPage" }, (data) => {

        }).done((data) => {
            if (data.auth && data.action === "results") {
                $("#editor").htmlcode(data.data);
            }
            else {
                window.alert("Virhe haettaessa vanhaa sisältöä. Yritä uudelleen myöhemmin");
            }
        });
    }
}



function switchToContactsEditorView() {
    let view = document.getElementById('contentView');

    if (view != null) {
        view.innerHTML = '<textarea id="editor"></textarea><br>' +
            '<form id="editForm">' +
            '<p><b>Huom!</b> Saat parhaimman lopputuloksen, jos kirjoitat tekstin ensin ja sen jälkeen lisäät muotoilut.</p>' +
            '<button class="btn btn-secondary" type="submit" id="submitContactsEdit">Tallenna muutokset</button>' +
            '<div id="editStatus"></div>' +
            '</form>';

        // switch tab style to active, deactivate others
        currentTab = "#contactsEditorTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });

        // submit edit
        $("#submitContactsEdit").click(function (event) {
            console.log("Edit submit click");
            event.preventDefault();
            if (confirm("Oletko aivan varma, että haluat tallentaa tiedot yhteystietosivulle? Toimintoa ei voi peruuttaa.")) {
                $.post("/content/edit", { edit: "contacts", html: $("#editor").htmlcode() }, function (data) {

                }).done(function (data) {
                    if (data.auth && data.action === "message" || data.auth && data.action === "error") {
                        //document.getElementById("editStatus").innerText = data.data;
                        window.alert(data.data);
                    }
                    else {
                        window.alert(data.data);
                        window.location.replace("/");
                    }
                });
            }
            else {
                window.alert("Toiminto peruutettiin, ja tietoja ei tallennettu.");
            }

        });

        // enable wysibb editor
        $("#editor").wysibb(wysibbOptions);

        // get current content
        $.get("/content/get", { type: "contacts" }, (data) => {

        }).done((data) => {
            if (data.auth && data.action === "results") {
                $("#editor").htmlcode(data.data);
            }
            else {
                window.alert("Virhe haettaessa vanhaa sisältöä. Yritä uudelleen myöhemmin");
            }
        });
    }
}

function switchToPage_1EditorView() {
    let view = document.getElementById('contentView');

    if (view != null) {
        view.innerHTML = '<textarea id="editor"></textarea><br>' +
            '<form id="editForm">' +
            '<p><b>Huom!</b> Saat parhaimman lopputuloksen, jos kirjoitat tekstin ensin ja sen jälkeen lisäät muotoilut.</p>' +
            '<button class="btn btn-secondary" type="submit" id="submitContactsEdit">Tallenna muutokset</button>' +
            '<div id="editStatus"></div>' +
            '</form>';

        // switch tab style to active, deactivate others
        currentTab = "#page_1EditorTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });

        // submit edit
        $("#submitContactsEdit").click(function (event) {
            console.log("Edit submit click");
            event.preventDefault();
            if (confirm("Oletko aivan varma, että haluat tallentaa tiedot käsikirjaan? Toimintoa ei voi peruuttaa.")) {
                $.post("/content/edit", { edit: "page_1", html: $("#editor").htmlcode() }, function (data) {

                }).done(function (data) {
                    if (data.auth && data.action === "message" || data.auth && data.action === "error") {
                        //document.getElementById("editStatus").innerText = data.data;
                        window.alert(data.data);
                    }
                    else {
                        window.alert(data.data);
                        window.location.replace("/");
                    }
                });
            }
            else {
                window.alert("Toiminto peruutettiin, ja tietoja ei tallennettu.");
            }

        });

        // enable wysibb editor
        $("#editor").wysibb(wysibbOptions);

        // get current content
        $.get("/content/get", { type: "page_1" }, (data) => {

        }).done((data) => {
            if (data.auth && data.action === "results") {
                $("#editor").htmlcode(data.data);
            }
            else {
                window.alert("Virhe haettaessa vanhaa sisältöä. Yritä uudelleen myöhemmin");
            }
        });
    }
}

function switchToPage_2EditorView() {
    let view = document.getElementById('contentView');

    if (view != null) {
        view.innerHTML = '<textarea id="editor"></textarea><br>' +
            '<form id="editForm">' +
            '<p><b>Huom!</b> Saat parhaimman lopputuloksen, jos kirjoitat tekstin ensin ja sen jälkeen lisäät muotoilut.</p>' +
            '<button class="btn btn-secondary" type="submit" id="submitContactsEdit">Tallenna muutokset</button>' +
            '<div id="editStatus"></div>' +
            '</form>';

        // switch tab style to active, deactivate others
        currentTab = "#page_2EditorTab";
        tabs.forEach((tab) => {
            if (tab !== currentTab) {
                $(tab).removeClass("active");
            }
            else {
                $(tab).addClass("active");
            }
        });

        // submit edit
        $("#submitContactsEdit").click(function (event) {
            console.log("Edit submit click");
            event.preventDefault();
            if (confirm("Oletko aivan varma, että haluat tallentaa tiedot ajankohtaista -sivulle? Toimintoa ei voi peruuttaa.")) {
                $.post("/content/edit", { edit: "page_2", html: $("#editor").htmlcode() }, function (data) {

                }).done(function (data) {
                    if (data.auth && data.action === "message" || data.auth && data.action === "error") {
                        //document.getElementById("editStatus").innerText = data.data;
                        window.alert(data.data);
                    }
                    else {
                        window.alert(data.data);
                        window.location.replace("/");
                    }
                });
            }
            else {
                window.alert("Toiminto peruutettiin, ja tietoja ei tallennettu.");
            }

        });

        // enable wysibb editor
        $("#editor").wysibb(wysibbOptions);

        // get current content
        $.get("/content/get", { type: "page_2" }, (data) => {

        }).done((data) => {
            if (data.auth && data.action === "results") {
                $("#editor").htmlcode(data.data);
            }
            else {
                window.alert("Virhe haettaessa vanhaa sisältöä. Yritä uudelleen myöhemmin");
            }
        });
    }
}







// wysibb (editor) options
let wysibbOptions = {
    lang: "en",
    buttons: "lihav,|,kursiv,|,allev,|,otsikko,|,img",
    allButtons: {
        otsikko: {
            title: "Muuta valittu teksti otsikoksi",
            buttonText: "Otsikko",
            transform: {
                '<h2>{SELTEXT}</h2>': '[otsikko]{SELTEXT}[/otsikko]'
            }
        },
        lihav: {
            title: "Muuta valittu teksti lihavoiduksi",
            buttonText: "Lihavoitu",
            transform: {
                '<b>{SELTEXT}</b>': '[lihavoitu]{SELTEXT}[/lihavoitu]'
            }
        },
        kursiv: {
            title: "Muuta valittu teksti kursivoiduksi",
            buttonText: "Kursivoitu",
            transform: {
                '<i>{SELTEXT}</i>': '[kursivoitu]{SELTEXT}[/kursivoitu]'
            }
        },
        allev: {
            title: "Muuta valittu teksti alleviivatuksi",
            buttonText: "Alleviivattu",
            transform: {
                '<u>{SELTEXT}</u>': '[alleviivattu]{SELTEXT}[/alleviivattu]'
            }
        }
    }
};
