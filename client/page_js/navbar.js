function loadNavbar(){
    if(document.getElementById('navbar') != null){
        document.getElementById('navbar').innerHTML =
        '<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" id="custom_navbar">' +
        '<nav class="container" id="navContainer">'+    
        '<a class="navbar-brand js-scroll-trigger" href="../"> <img src="../img/kimppalogo.png" id="logo"> </a>' +
            '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsResponsive" aria-controls="navbarsResponsive" aria-expanded="false" aria-label="Toggle navigation">' +
                '<span class="navbar-toggler-icon"></span>' +
            '</button>' +
            
            '<div class="collapse navbar-collapse" id="navbarsResponsive">' +
                '<ul class="navbar-nav mr-auto">' +
                    '<li class="nav-item active" id="hide1">' +
                        '<a class="nav-link" href="../">Etusivu <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                    '<li class="nav-item active d-none" id="hide2">' +
                        '<a class="nav-link" href="../kimppatapahtumat">Kimppatapahtumat <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                    '<li class="nav-item active d-none" id="hide3">' +
                        '<a class="nav-link" href="../kamutapahtumat">Kamutapahtumat <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                    '<li class="nav-item active">' +
                        '<a class="nav-link" href="../yhteystiedot">Yhteystiedot <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                    '<li class="nav-item active d-none" id="hide4">' +
                        '<a class="nav-link" href="../kasikirja">Kamun käsikirja <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                    '<li class="nav-item active d-none" id="hide5">' +
                        '<a class="nav-link" href="../ajankohtaista">Ajankohtaista <span class="sr-only">(current)</span></a>' +
                    '</li>' +
                '</ul>' +

                '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle disabled" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    'Et ole kirjautuneena sisään' +
                    '</button>' +
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                       '<a class="dropdown-item d-none" id="maintenanceButton" href="../yllapito">Ylläpito</a>' +
                        '<a class="dropdown-item d-none" id="userEventsButton" href="../omatkimppatapahtumat">Omat kimppatapahtumat</a>' +
                        '<a class="dropdown-item d-none" id="userEventsButton2" href="../omatkamutapahtumat">Omat kamutapahtumat</a>' +
                        '<a class="dropdown-item" id="userDetailsButton" href="../tilitiedot">Käyttäjätiedot</a>' +
                        '<a class="dropdown-item" id="logOutButton" href="#" onclick="sendLogout();">Kirjaudu ulos</a>' +
                    '</div>' +
                '</div>' +
                
            '</div>' +
            '</nav>' +
        '</nav>';
    }
   
}

function sendLogout() {
    $.get('/logout', (data) => {

    }).done((data) => {
        window.location.replace("/");
    });
}