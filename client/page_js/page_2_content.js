//page 2 content
function page_2Content(){
 
    if(document.getElementById('page_2_content') != null){
    document.getElementById('page_2_content').innerHTML =

        '<div class="container" id="wider_container">' +
            '<div class="row" id="index_info">' +
                '<div class="col-lg-12" id="column_wide">' +
                    '<p><div class="mw-50 mh-50" id="editablePage_2Content"></div></p>' +
                '</div>' +
                '</div>' +
            '</div>' +
            '<hr>'
        '</div>';
    }
}