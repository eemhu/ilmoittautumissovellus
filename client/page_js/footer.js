//footer content
function footer(){
    if(document.getElementById('footer') != null){
        document.getElementById('footer').innerHTML =

        '<footer class="footer mt-auto py-3" id="footer">' +
			'<div class="container" id="footerContainer">' +
				'<div class="row text-center text-xs-center text-sm-left text-md-left">' +
					'<div class="col-xs-12 col-sm-6 col-md-6">' +
						'<h5>Linkkejä</h5>' +
						'<ul class="list-unstyled quick-links">' +
							'<li><a href="https://www.facebook.com/kulttuurikimppa/"><i class="fa fa-angle-double-right"></i>Facebook</a></li>' +
						'</ul>' +
					'</div>' +
					'<div class="col-xs-12 col-sm-6 col-md-6">' +
						//'<h5>Quick links</h5>' +
						'<ul class="list-unstyled quick-links">' +
							'<li><a href="#"><i class="fa fa-angle-double-right"></i><b>KULTUURIKIMPPA - yhteisöllisiä kulttuurielämyksiä Joensuussa.</b></a></li>' +
							'<li><a href="#"><i class="fa fa-angle-double-right"></i><b>CULTURE CREW - opportunities for communal culture experiences for everyone in the Joensuu area. Welcome to the crew!</b></a></li>' +
							'<li><a href="#"><i class="fa fa-angle-double-right"></i>www.joensuu.fi/kulttuurikimppa</a></li>' +
						'</ul>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</footer>';
    }
}