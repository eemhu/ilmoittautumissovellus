# Ilmoittautumissovellus

Ilmoittautumissovellus Joensuun kaupungille

## Käytäthän "development" node environmentia.

Huom. Development mode poistaa sähköpostin lähettämisen käytöstä. Käynnistä palvelin "production" environmentissa testataksesi sähköpostin lähettämistä.
(Production environmentin saa käyttöön korvaamalla alla olevissa ohjeissa 'development' 'production'lla, tai jos NODE_ENV ympäristömuuttujaa ei ole olemassa.)

Ohjeet (Windows):
```
set NODE_ENV=development
::nodemon server.js
::npm start
::node server.js
:: Kommentoi pois se mitä käynnistystapaa käytät.
```

Ohjeet (Linux):
```
export NODE_ENV=development
#nodemon server.js
#npm start
#node server.js
# Kommentoi pois se mitä käynnistystapaa käytät.
```